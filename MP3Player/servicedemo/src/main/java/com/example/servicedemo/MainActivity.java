package com.example.servicedemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import static com.example.servicedemo.R.id.cancel_action;
import static com.example.servicedemo.R.id.startservice1;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    Button startbutton1,stopbutton1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startbutton1= (Button) findViewById(R.id.startservice1);
        stopbutton1= (Button) findViewById(R.id.stopservice1);

        startbutton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "startservice1" );
                Intent startintent = new Intent(MainActivity.this,MyService.class);
                startintent.putExtra("test",100);
                startService(startintent);
            }
        });
        stopbutton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "stopservice1" );
                Intent stopintent = new Intent(MainActivity.this,MyService.class);
                stopService(stopintent);
                //원래는 이렇게 하면 안되고 만들어놓은 인텐트를 잘 가지고 있다가 모셔놓은거를 그걸로 멈춰야 함.이건 내용물이 비슷해서
                //ㄱㅊ
            }
        });

//        startbutton1.setOnClickListener(this );
    }

//    public  void onClick(View view){
//        switch (view.getId()){
//            case R.id.startservice1:
//                Log.e(TAG, "startservice1" );
//                break;
//        }
//    }
}
