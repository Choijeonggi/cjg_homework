package com.example.servicedemo;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {

   public static final String TAG = MyService.class.getSimpleName();

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int number = 100;

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "service created" );
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand    " +intent.getIntExtra("test",-1));
        return START_STICKY;//이러면 서비스가 다시살아나지 않음,, stickycompativity?,,은 죽여도 살아있음,,


    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "Destroy service" );
        super.onDestroy();
    }
}
