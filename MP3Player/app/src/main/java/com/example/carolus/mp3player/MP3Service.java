package com.example.carolus.mp3player;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

public class MP3Service extends Service {
    public static final String TAG = MP3Service.class.getSimpleName();

    public static final String ACTION_PLAY_OR_STOP = "PLAYORSTOP";
    public static final String ACTION_PREV = "PREV";
    public static final String ACTION_NEXT = "NEXT";

    private static final int REQ_MAINACTIVITY = 7;
    private static final int REQ_PLAY_OR_STOP = 8;
    private static final int REQ_PREV = 60;
    private static final int REQ_NEXT = 50;

    private MediaPlayer mediaPlayer = null;
    private MP3Binder mp3Binder = new MP3Binder();

    private MP3Adapter mp3Adapter = new MP3Adapter();

    private MP3Data currentData = null;
    private int state = MP3State.STATE_NONE;


    @Override
    public IBinder onBind(Intent intent) {
        return mp3Binder;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate: ");
        super.onCreate();
        refresh();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand: ");
        //action비교
        if (!TextUtils.isEmpty(intent.getAction())) {
            if (intent.getAction().equals(ACTION_PLAY_OR_STOP)) {
                playOrStop();
            } else if (intent.getAction().equals(ACTION_PREV)) {
                previous();
            } else if (intent.getAction().equals(ACTION_NEXT)) {
                next();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy: ");
        super.onDestroy();
    }

    public ArrayList<MP3Data> refresh() {
        ArrayList<MP3Data> items = new ArrayList<>();
        items.clear();
        final Cursor cursor = getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null, null, null, null
        );

        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
            String song = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
            String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
            long albumid = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));

            MP3Data mp3Data = new MP3Data();
            mp3Data.artist = artist;
            mp3Data.song = song;
            mp3Data.id = id;

            Cursor cursor1th = getContentResolver().query(
                    MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Audio.Albums.ALBUM_ART},
                    MediaStore.Audio.Albums._ID + "=?",
                    new String[]{String.valueOf(albumid)},
                    null
            );
            if (cursor1th.moveToNext()) {
                String thumbpath = cursor1th.getString(cursor1th.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
                if (!TextUtils.isEmpty(thumbpath)) {
                    mp3Data.thumbnail = thumbpath;
                }
            }
            items.add(mp3Data);
        }

        mp3Adapter.setItems(items);
        return items;
    }


    class MP3Binder extends Binder {
        MP3Service getService() {
            return MP3Service.this;
        }
    }

    public void playOrStop() {
        switch (state) {

            case MP3State.STATE_PLAYING:
                mediaPlayer.pause();
                state = MP3State.STATE_PAUSE;
                break;
            case MP3State.STATE_PAUSE:
                mediaPlayer.start();
                state = MP3State.STATE_PLAYING;
                break;
        }
    }

    public void previous() {
        switch (state) {

            case MP3State.STATE_PLAYING:
            case MP3State.STATE_PAUSE:
                int currentPosition = mp3Adapter.getItems().indexOf(currentData);
                int newPosition = currentPosition - 1;
                if (newPosition < 0) {
                    newPosition = mp3Adapter.getCount() - 1;
                }
                loadMusic(mp3Adapter.getItem(newPosition));
                break;

        }
    }

    public void next() {
        switch (state) {

            case MP3State.STATE_PLAYING:
            case MP3State.STATE_PAUSE:
                int currentPosition = mp3Adapter.getItems().indexOf(currentData);
                int newPosition = currentPosition + 1;
                if (newPosition >= mp3Adapter.getCount()) {
                    newPosition = 0;
                }
                loadMusic(mp3Adapter.getItem(newPosition));
                break;
        }

    }

    public void loadMusic(MP3Data mp3Data) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        if (currentData != null) {
            if (currentData.equals(mp3Data)) {
                return;
            }
        }


        mediaPlayer = new MediaPlayer();
        try {
            Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, mp3Data.id);

            mediaPlayer.setDataSource(MP3Service.this, uri);
//            mediaPlayer.prepare(); //쓰레드를 멈추게 함
            state = MP3State.STATE_PREPARING;
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                    state = MP3State.STATE_PLAYING;//쓰레드 동기화 고려하지 않음
                }
            });


            Intent playOrStopIntent = new Intent(MP3Service.this, MP3Service.class);
            playOrStopIntent.setAction(ACTION_PLAY_OR_STOP);

            Intent prevIntent = new Intent(MP3Service.this, MP3Service.class);
            prevIntent.setAction(ACTION_PREV);

            Intent nextIntent = new Intent(MP3Service.this, MP3Service.class);
            nextIntent.setAction(ACTION_NEXT);

            Bitmap icon = getResizedAlbumImage(mp3Data.thumbnail);

            Notification notification = new NotificationCompat.Builder(this)
                    .setContentTitle(mp3Data.song)
                    .setContentText(mp3Data.artist)
                    .setSmallIcon(R.drawable.default_icon)
                    .setLargeIcon(icon)
                    .setContentIntent(
                            PendingIntent.getActivity(MP3Service.this, REQ_MAINACTIVITY,
                                    new Intent(MP3Service.this, MainActivityMP3.class),
                                    PendingIntent.FLAG_UPDATE_CURRENT)
                    )

                    .addAction(android.R.drawable.ic_media_previous, "Prev",
                            PendingIntent.getService(
                                    MP3Service.this,
                                    REQ_PREV,
                                    prevIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                            )
                    )
                    .addAction(android.R.drawable.ic_media_play, "Play",
                            PendingIntent.getService(
                                    MP3Service.this,
                                    REQ_PLAY_OR_STOP,
                                    playOrStopIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                            )
                    )
                    .addAction(android.R.drawable.ic_media_next, "Next",
                            PendingIntent.getService(
                                    MP3Service.this,
                                    REQ_NEXT,
                                    nextIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                            )
                    )
                    .build();
            startForeground(1, notification);
            //비트맵은할당한 메모리를 프로그래머가 해제 해줘야 함, 안그러면 메모리 누수
            if (icon != null) {
                icon.recycle();
            }

        } catch (IOException e) {
            currentData = null;
            mediaPlayer = null;
            state = MP3State.STATE_NONE;
            e.printStackTrace();
        }
        currentData = mp3Data;

    }


    public Bitmap getResizedAlbumImage(String path) {


        DisplayMetrics dm = getResources().getDisplayMetrics();

        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, option);
        int thumbnailW = option.outWidth;
        int thumbnailH = option.outHeight;

        int scaleFactor = Math.min(thumbnailW / (int) (64f * dm.density), thumbnailH / (int) (64f * dm.density));

        option.inJustDecodeBounds = false;
        option.inSampleSize = scaleFactor;
        option.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path, option);
        return bitmap;

        //내폰의 밀도 그룹이 6개중 어디에 속해있는지 알아내기

//               Log.e(TAG,"xdpi:"+dm.xdpi);
//               Log.e(TAG,"ydpi:"+dm.ydpi);
//               Log.e(TAG,"widthPixels:"+dm.widthPixels);
//               Log.e(TAG,"heightPixels:"+dm.heightPixels);
//               Log.e(TAG,"density:"+dm.density);
//               Log.e(TAG,"densityDpi:"+dm.densityDpi);
//               Log.e(TAG,"scaledDensity:"+dm.scaledDensity);
    }

    public MP3Adapter getMp3Adapter() {
        return mp3Adapter;
    }

}
