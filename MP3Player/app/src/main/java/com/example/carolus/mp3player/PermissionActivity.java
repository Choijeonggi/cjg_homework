package com.example.carolus.mp3player;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

/**
 * Created by carolus on 2017-06-18.
 */

public class PermissionActivity extends AppCompatActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new TedPermission(this)
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        finish();
                        Intent intent = new Intent(PermissionActivity.this,MainActivityMP3.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                        finish();
                    }
                })
                .check();
    }
}
