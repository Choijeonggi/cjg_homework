package com.example.carolus.mp3player;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by carolus on 2017-06-04.
 */

public class MP3Data implements Parcelable {
    public long id;
    public String thumbnail;
    public String song;
    public String artist;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.thumbnail);
        dest.writeString(this.song);
        dest.writeString(this.artist);
    }

    public MP3Data() {
    }

    protected MP3Data(Parcel in) {
        this.id = in.readLong();
        this.thumbnail = in.readString();
        this.song = in.readString();
        this.artist = in.readString();
    }

    public static final Parcelable.Creator<MP3Data> CREATOR = new Parcelable.Creator<MP3Data>() {
        @Override
        public MP3Data createFromParcel(Parcel source) {
            return new MP3Data(source);
        }

        @Override
        public MP3Data[] newArray(int size) {
            return new MP3Data[size];
        }
    };
}
