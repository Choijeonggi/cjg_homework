package com.example.carolus.mp3player;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

public class MainActivityMP3 extends AppCompatActivity {


    public static final String TAG = MainActivityMP3.class.getSimpleName();

    ListView mMp3listView;
    ImageButton playorpausebtn, nextbtn, previousbtn;



    private MP3Adapter mp3Adapter = null;

    private boolean mBound = false;
    private MP3Service mp3Service = null;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.e(TAG, "onserviceConnected");
            mBound = true;
            mp3Service = ((MP3Service.MP3Binder) service).getService();
            mp3Adapter=mp3Service.getMp3Adapter();
            mMp3listView.setAdapter(mp3Adapter);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mp3);
        mMp3listView = (ListView) findViewById(R.id.MP3list_main);
        playorpausebtn = (ImageButton) findViewById(R.id.play_or_pause_main);
        nextbtn = (ImageButton) findViewById(R.id.next_main);
        previousbtn = (ImageButton) findViewById(R.id.previous_main);




        mMp3listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mp3Service.loadMusic(mp3Adapter.getItem(position));


            }
        });

        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3Service.next();
            }
        });

        playorpausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3Service.playOrStop();
            }
        });

        previousbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3Service.previous();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mBound) {
            Intent intent = new Intent(MainActivityMP3.this, MP3Service.class);
            startService(intent);
            bindService(intent, connection, Service.BIND_AUTO_CREATE);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBound) {
            unbindService(connection);
            mBound = false;
            mp3Service = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
               mp3Service.refresh();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
