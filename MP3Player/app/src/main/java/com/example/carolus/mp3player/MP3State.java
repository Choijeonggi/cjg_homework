package com.example.carolus.mp3player;

/**
 * Created by carolus on 2017-06-11.
 */

public class MP3State {
    public static final int STATE_NONE=10;
    public static final int STATE_PLAYING=5;
    public static final int STATE_PAUSE=1;
    public static final int STATE_PREPARING=11;

}
