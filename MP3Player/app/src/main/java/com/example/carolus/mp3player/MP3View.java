package com.example.carolus.mp3player;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

/**
 * Created by carolus on 2017-06-04.
 */

public class MP3View extends FrameLayout {

    ImageView thumbnail;
    TextView songname,artistname;
     MP3Data data;

    public MP3View(@NonNull Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.mp3_view,this,true);
        thumbnail= (ImageView) view.findViewById(R.id.thumbnail_view);
        songname= (TextView) view.findViewById(R.id.song_view);
        artistname= (TextView) view.findViewById(R.id.artist_view);

    }

    public void setData(MP3Data data) {
        this.data = data;
        songname.setText(data.song);
        artistname.setText(data.artist);
        if (!TextUtils.isEmpty(data.thumbnail)){
            thumbnail.setImageURI(Uri.fromFile(new File(data.thumbnail)));
        }
    }
}
