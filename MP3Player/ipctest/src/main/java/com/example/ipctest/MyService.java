package com.example.ipctest;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MyService extends Service {

    public static final String TAG = MyService.class.getSimpleName();

    //전역 변수는 클래스 호출시 초기화 됨, 바인더 만들기
    private MyBinder mBinder= new MyBinder();



    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG,"onBind");
        //MainActivity가 bindService를 할때 호출됨
        //메인액티비티에 바인더를 넘겨줌 이 서비스와 통신하기 위해
        return mBinder;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG,"OnCreate Service");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG,"onStartCommand");
        return Service.START_NOT_STICKY;
    }



    public void helloWorld(){
        Toast.makeText(this, "Hello World", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG,"Service onDestroy");
    }


    //액티비티와 연결하기 위한 바인더 클래스 생성
    class MyBinder extends Binder{
        MyService getSerivce(){
            Log.e(TAG,"getSerivce");
            return MyService.this;
        }
    }
}
