package com.example.ipctest;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
public static final String TAG = MainActivity.class.getSimpleName();
    //서비스 연결 성공여부 플래그
    private boolean mBound = false;
    private MyService myService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,"onCreate");



        setContentView(R.layout.activity_main);

        Button test = (Button) findViewById(R.id.test);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myService.helloWorld();
            }
        });
        Button bind = (Button) findViewById(R.id.bind);
        bind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,MyService.class);
                startService(intent);
                //서비스가 실행되어있지 않다면 서비스를 실행하고 바인드 시킴
                //서비스가 실행되어있다면 바인드만함.
                //바인드 = 액티비티와 서비스의 연결
                bindService(intent,connection, Service.BIND_AUTO_CREATE);
            }
        });
         Button unbind = (Button) findViewById(R.id.unbind);
        unbind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBound){
                    Log.e(TAG,"unbind service");

                    unbindService(connection);
                    stopService(new Intent(MainActivity.this,MyService.class));

                    mBound=false;
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG,"Activity onResume");
//        //서비스 연결(바운드)
//        Intent intent = new Intent(MainActivity.this,MyService.class);
//        //서비스 연결 시도
//        startService(intent);
//        bindService(intent,connection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {

        super.onPause();
        Log.e(TAG,"Activity onPause");
//        //서비스 연결해제 (언바운드)
//
//
//        if(mBound){
//            unbindService(connection);
//        }




    }




    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            //서비스 연결 성공시
            Log.e(TAG,"onServiceConnected");
            mBound = true;
            myService = ((MyService.MyBinder)service).getSerivce();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
             //서비스 연결이 시스템에 의해 끊길시
            Log.e(TAG,"onServiceDisconnected");

            mBound = false;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG,"onDestroy");
    }
}
