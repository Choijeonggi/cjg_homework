package com.carolus.listviewstudy;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.LayoutInflaterCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by carolus on 2017-04-09.
 */

public class ItemView extends FrameLayout {

    private TextView titleView;
    private ImageView iconView;
    private Button button;
    private ItemData data;
    private Context context;

    public ItemView(@NonNull Context context) {
        super(context);
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.item_view,this,true);

        titleView = (TextView) view.findViewById(R.id.title);
        iconView = (ImageView) view.findViewById(R.id.icon);
        button = (Button) findViewById(R.id.btn);

        //원래 이렇게 하면 안되는 코드 하지만 간단함.......
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ItemView.this.context,data.name, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void setData(ItemData data){
        this.data=data;
        titleView.setText(data.name);
        iconView.setImageResource(data.icon_id);

    }
}
