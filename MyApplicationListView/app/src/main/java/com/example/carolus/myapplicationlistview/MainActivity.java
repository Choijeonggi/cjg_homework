package com.example.carolus.myapplicationlistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    GridView gridView;

    MyAdapter adapter;
    String[] data = new String[]{"1","2","4","3","5"};
    int[] imageData = new int[]{R.drawable.alpha,R.drawable.beta,R.drawable.cupcake,R.drawable.donut,R.drawable.eclaire};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView= (GridView) findViewById(R.id.gridview);


        final ArrayList<ListData> items=new ArrayList<>();

        for (int i=0;i<data.length;i++){
            ListData listData = new ListData();
            listData.id=i;
            listData.icon_id=imageData[i];
            items.add(listData);
        }


        adapter=new MyAdapter();
        gridView.setAdapter(adapter);
        adapter.setItems(items);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, ((ListData)adapter.getItem(position)).string, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
