package com.example.carolus.myapplicationlistview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by carolus on 2017-05-28.
 */

public class MyView extends FrameLayout {

    private TextView text;
    private ImageView image;
    private Context context;
    private ListData data;


    public MyView(@NonNull Context context) {
        super(context);
        this.context=context;
        View view = LayoutInflater.from(context).inflate(R.layout.grid_view,this,true);

        text= (TextView) view.findViewById(R.id.text);
        image= (ImageView) view.findViewById(R.id.image);
    }


    public void setData(ListData data) {
        this.data = data;
        text.setText(data.string);
        image.setImageResource(data.icon_id);
    }
}
