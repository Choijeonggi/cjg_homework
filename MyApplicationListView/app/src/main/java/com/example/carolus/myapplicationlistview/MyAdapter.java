package com.example.carolus.myapplicationlistview;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by carolus on 2017-05-28.
 */

public class MyAdapter extends BaseAdapter {

    private ArrayList<ListData> items = new ArrayList<>();

    public void setItems(ArrayList<ListData> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView=new MyView(parent.getContext());

        }

        ((MyView)convertView).setData(items.get(position));




        return convertView;
    }
}
