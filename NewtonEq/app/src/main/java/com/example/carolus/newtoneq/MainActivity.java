package com.example.carolus.newtoneq;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText constantVal=null;
    EditText xVal=null;
    TextView yVal;
    Button calBtn;
long c,x ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        constantVal = (EditText) findViewById(R.id.constant);
        xVal = (EditText) findViewById(R.id.x);
        yVal = (TextView) findViewById(R.id.y);
        calBtn= (Button) findViewById(R.id.calBtn);

//        constantVal.setText(""+0);
//        xVal.setText(""+0);


calBtn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

            c = Long.parseLong(constantVal.getText().toString());
            x = Long.parseLong(xVal.getText().toString());

            yVal.setText("" + c * x);

    }
});


    }
}
