package firsthomework;

import java.util.Scanner;
import java.util.Arrays;
public class ChangedString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 // 역순 정렬
		Scanner word = new Scanner(System.in);
		String arr = word.next();
		  char[] array = arr.toCharArray();
		  
		  int temp=0;
		  int j=array.length-1;
		  
		  for(int i=0; i<array.length/2; i++, j--){ 
		  // 절반까지만 바꾸어주기 때문에 조건을 배열/2를 해준다.
		   temp = array[i];
		   array[i]=array[j];
		   array[j]=(char)temp;

		  }
		  
		  for(int i=0; i<array.length; i++){ //역순으로 바뀐 배열을 출력
		   System.out.print(array[i]);
		  }
	}


	}