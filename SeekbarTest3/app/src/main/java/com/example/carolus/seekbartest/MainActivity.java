package com.example.carolus.seekbartest;

import android.Manifest;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();
    TextView text, text2;
    SeekBar seekBar;
    Button startBtn;
    MediaPlayer mediaPlayer;

    private ReentrantLock lock = new ReentrantLock();

    Thread mediaPlayerWatcher = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    if (!lock.isLocked()) {
                        int t = mediaPlayer.getCurrentPosition();
                        handler.sendEmptyMessage(t);
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    });

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int t = msg.what;
            seekBar.setProgress(t);
            text.setText(getTime(t));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.text);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        startBtn = (Button) findViewById(R.id.startBtn);
        text2 = (TextView) findViewById(R.id.text2);


        new TedPermission(this)
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        MediaPlayer mp = loadMusicFromAsset();
                        if (mp == null) {
                            Log.e(TAG, "에러 , 파일 로드실패");
                            return;
                        }
                        mediaPlayer = mp;
                        seekBar.setMax(mediaPlayer.getDuration());
                        text2.setText(getTime(mediaPlayer.getDuration()));
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                        finish();
                    }
                })
                .check();


        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.start();
                mediaPlayerWatcher.start();
            }
        });


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                text.setText(getTime(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                lock.lock();

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                mediaPlayer.seekTo(progress);
                lock.unlock();
            }
        });


    }

    private MediaPlayer loadMusicFromAsset() {

        AssetManager assetManager = getAssets();
        try {
            AssetFileDescriptor afd = assetManager.openFd("music.mp3");

            MediaPlayer mediaPlayer1 = new MediaPlayer();
            mediaPlayer1.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer1.prepare();
            return mediaPlayer1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getTime(int time){
        int m = time/60000;
        int s = time/1000%60;
        return String.format("%02d:%02d",m,s);
    }
}
