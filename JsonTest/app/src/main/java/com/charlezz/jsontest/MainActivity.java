package com.charlezz.jsontest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    EditText id, name, age;
    EditText jsonEditText;

    Button convert1, convert2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        id = (EditText) findViewById(R.id.id);
        name = (EditText) findViewById(R.id.name);
        age = (EditText) findViewById(R.id.age);
        jsonEditText = (EditText) findViewById(R.id.jsonEditText);

        convert1 = (Button) findViewById(R.id.convert1);
        convert2 = (Button) findViewById(R.id.convert2);


        convert1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strId = id.getText().toString();
                String strName = name.getText().toString();
                int intAge = Integer.parseInt(age.getText().toString());

                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("id", strId);
                    jsonObject.put("name", strName);
                    jsonObject.put("age", intAge);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                jsonEditText.setText(jsonObject.toString());

            }
        });

        convert2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonEditText.getText().toString());
                    id.setText(jsonObject.getString("id"));
                    name.setText(jsonObject.getString("name"));
                    age.setText(String.valueOf(jsonObject.getInt("age")));


//                    JSONArray jsonArray = new JSONArray("[{\"id\":\"abc\",\"age\":200,\"name\":\"Dunkin\"},{\"id\":\"abc\",\"age\":200,\"name\":\"Dunkin\"},{\"id\":\"abc\",\"age\":200,\"name\":\"Dunkin\"}]");
//
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject obj = jsonArray.getJSONObject(i);
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }
}
