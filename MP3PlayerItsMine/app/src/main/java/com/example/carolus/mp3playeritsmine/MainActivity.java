package com.example.carolus.mp3playeritsmine;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.locks.ReentrantLock;

public class MainActivity extends AppCompatActivity implements IStateChange {
    public static final String TAG = MainActivity.class.getSimpleName();

    ListView mp3List;
    ImageButton prevBtn, nextBtn, playPauseBtn, randomPlay, SongAgain;

    TextView mainSongname, mainArtistName, currentSongPlayTime, maxPlayTime;
    SeekBar playBar;


    private Mp3Adapter mp3Adapter = null;

    private Mp3Service mp3Service = null;

    private boolean mbound = false;

    private long backPressedTime = 0;




    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mbound = true;
            mp3Service = ((Mp3Service.Mp3Binder) service).getService();
            mp3Adapter = mp3Service.getMp3Adapter();
            mp3List.setAdapter(mp3Adapter);
            changeImage();
            if (mp3Service.getCurrentMp3Data() != null) {
                playBar.setMax(mp3Service.playbarMaxTime);
                maxPlayTime.setText(mp3Service.playbarMaxTime);
            }
            mp3Service.setOnStateChangedListener(MainActivity.this);


        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mbound = false;
            mp3Service.setOnStateChangedListener(null);
        }
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mp3List = (ListView) findViewById(R.id.Mp3ListMain);
        prevBtn = (ImageButton) findViewById(R.id.PreviousBtnMain);
        playPauseBtn = (ImageButton) findViewById(R.id.PlayOrPauseBtnMain);
        nextBtn = (ImageButton) findViewById(R.id.NextBtnMain);
        randomPlay = (ImageButton) findViewById(R.id.randomPlay);
        SongAgain = (ImageButton) findViewById(R.id.SongAgain);
        playBar = (SeekBar) findViewById(R.id.playBar);
        mainArtistName = (TextView) findViewById(R.id.MainArtistName);
        mainSongname = (TextView) findViewById(R.id.MainSongname);
        currentSongPlayTime = (TextView) findViewById(R.id.currentSongPlayTime);
        maxPlayTime = (TextView) findViewById(R.id.maxPlayTime);

        mp3List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mp3Service.listPosition = position;
                mp3Service.buttonState = Mp3State.BUTTON_STATE_MP3LIST;
                mp3Service.playByMp3ListClicked();
                playPauseBtn.setImageResource(R.drawable.ic_media_pause);
                changeImage();
//                playBar.setMax(mp3Service.playbarMaxTime);
//                new ShiftPoint().start();

            }
        });

        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3Service.buttonState = Mp3State.BUTTON_STATE_PREVBTN;
                mp3Service.previous();
                playPauseBtn.setImageResource(R.drawable.ic_media_pause);
                changeImage();
            }
        });

        playPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3Service.playAndPause();
                playPauseBtn.setImageResource(R.drawable.ic_media_pause);
                changeImage();

            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3Service.buttonState = Mp3State.BUTTON_STATE_NEXTBTN;
                mp3Service.next();
                playPauseBtn.setImageResource(R.drawable.ic_media_pause);
                changeImage();

            }
        });

        randomPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mp3Service.shuffleState) {
                    case Mp3State.STATE_DISABLE_SHUFFLE:
                        mp3Service.shuffleState = Mp3State.STATE_ENABLE_SHUFFLE;
                        mp3Service.enableRandom();
                        break;
                    case Mp3State.STATE_ENABLE_SHUFFLE:
                        mp3Service.shuffleState = Mp3State.STATE_DISABLE_SHUFFLE;
                        mp3Service.disableRandom();
                        break;
                }

            }
        });

        SongAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mp3Service.playSongAgainState) {
                    case Mp3State.STATE_JUST_PLAY:
                        mp3Service.playSongAgainState = Mp3State.STATE_SONG_AGAIN;
                        mp3Service.playAgain();
                        break;
                    case Mp3State.STATE_SONG_AGAIN:
                        mp3Service.playSongAgainState = Mp3State.STATE_JUST_PLAY;
                        mp3Service.disableRandom();
                        break;
                }


            }
        });


        playBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
               // playBar.setMax(mp3Service.getMediaPlayer().getDuration());
              //  maxPlayTime.setText(getTime(mp3Service.getMediaPlayer().getDuration()));
                currentSongPlayTime.setText(getTime(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mp3Service.startTouchPlaybar=true;
                mp3Service.getLock().lock();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = playBar.getProgress();
                mp3Service.startTouchPlaybar=false;
                mp3Service.tossedProgress(progress);
                mp3Service.getLock().unlock();
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mbound) {
            Intent intent = new Intent(MainActivity.this, Mp3Service.class);
            startService(intent);
            bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mbound) {
            mp3Service.setOnStateChangedListener(null);
            unbindService(serviceConnection);
            mbound = false;
            mp3Service = null;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                mp3Service.refresh();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeImage() {
        switch (mp3Service.getState()) {
            case Mp3State.STATE_PLAYING:
                playPauseBtn.setImageResource(R.drawable.ic_media_pause);
                break;
            case Mp3State.STATE_NONE:
            case Mp3State.STATE_PAUSE:
                playPauseBtn.setImageResource(R.drawable.ic_media_play);
                break;
        }
        if (mp3Service.getCurrentMp3Data() == null) {
            mainArtistName.setText("song is not running.");
            mainSongname.setText("song is not running.");
        } else if (mp3Service.getCurrentMp3Data() != null) {
            mainSongname.setText(mp3Service.getCurrentMp3Data().songtitle);
            mainArtistName.setText(mp3Service.getCurrentMp3Data().artistname);

        }
    }

    @Override
    public void onStateChanged(int state) {
        switch (state) {
            case Mp3State.STATE_PLAYING:
                playPauseBtn.setImageResource(R.drawable.ic_media_pause);
                break;
            case Mp3State.STATE_NONE:
            case Mp3State.STATE_PAUSE:
                playPauseBtn.setImageResource(R.drawable.ic_media_play);
                break;

        }

    }

    @Override
    public void onMp3DataChanged(Mp3Data mp3Data) {
        if (mp3Data == null) {
            mainArtistName.setText("song is not running.");
            mainSongname.setText("song is not running.");
        } else if (mp3Data != null) {
            mainSongname.setText(mp3Data.songtitle);
            mainArtistName.setText(mp3Data.artistname);

        }
    }

    @Override
    public void playbarStateChanged(int maxProgress) {
        playBar.setMax(maxProgress);
        maxPlayTime.setText(getTime(maxProgress));

    }

    @Override
    public void handlerValueChanged(int nowTime) {
        playBar.setProgress(nowTime);
        currentSongPlayTime.setText(getTime(nowTime));
    }

    @Override
    public void onBackPressed() {
        long pressedTime = System.currentTimeMillis();

        if (pressedTime > backPressedTime + Mp3State.FINISH_INTERVAL_TIME) {
            backPressedTime = pressedTime;
            Toast.makeText(this, "App will finish if you press once again", Toast.LENGTH_SHORT).show();
            return;
        }
        if (pressedTime <= backPressedTime + Mp3State.FINISH_INTERVAL_TIME) {
            Intent intent = new Intent(MainActivity.this, Mp3Service.class);
            stopService(intent);
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    private String getTime(int time) {
        int m = time / 60000;
        int s = time / 1000 % 60;
        return String.format("%02d:%02d", m, s);
    }
}
