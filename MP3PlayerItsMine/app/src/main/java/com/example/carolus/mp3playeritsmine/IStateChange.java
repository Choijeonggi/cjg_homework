package com.example.carolus.mp3playeritsmine;

/**
 * Created by carolus on 2017-07-02.
 */

public interface IStateChange {

    void onStateChanged(int state);

    void onMp3DataChanged(Mp3Data mp3Data);

    void playbarStateChanged(int maxProgress);

    void handlerValueChanged(int nowTime);

}
