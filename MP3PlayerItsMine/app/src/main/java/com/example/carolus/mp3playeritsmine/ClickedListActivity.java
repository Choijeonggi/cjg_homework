package com.example.carolus.mp3playeritsmine;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by carolus on 2017-07-12.
 */

public class ClickedListActivity extends AppCompatActivity {

    ImageView subImage;
    TextView subSongTitle, subArtistName;
    SeekBar subPlayBar;
    ImageButton subPrevBtn,subPlayAndPauseBtn,subNextBtn,subRandomBtn,subSongAgainBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clicked_list_activity);

        subImage= (ImageView) findViewById(R.id.SubImage);
        subSongTitle= (TextView) findViewById(R.id.SubSongTitle);
        subArtistName= (TextView) findViewById(R.id.SubArtistName);
        subPlayBar= (SeekBar) findViewById(R.id.SubPlayBar);
        subPrevBtn= (ImageButton) findViewById(R.id.SubPrevBtn);
        subPlayAndPauseBtn= (ImageButton) findViewById(R.id.SubPlayAndPauseBtn);
        subNextBtn= (ImageButton) findViewById(R.id.SubNextBtn);
        subRandomBtn= (ImageButton) findViewById(R.id.SubRandomBtn);
        subSongAgainBtn= (ImageButton) findViewById(R.id.SubSongAgainBtn);




    }
}
