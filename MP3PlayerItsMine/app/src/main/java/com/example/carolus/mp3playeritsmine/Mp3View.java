package com.example.carolus.mp3playeritsmine;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

/**
 * Created by carolus on 2017-06-27.
 */

public class Mp3View extends FrameLayout {

    ImageView thumbnailImage;
    TextView song,artist;
    Mp3Data mp3Data;

    public Mp3View(@NonNull Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.mp3_view,this,true);
        thumbnailImage= (ImageView) view.findViewById(R.id.ThumbnailImage_view);
        song= (TextView) view.findViewById(R.id.songname_view);
        artist= (TextView) view.findViewById(R.id.artistname_view);
    }

    public void setMp3Data(Mp3Data mp3Data) {
        this.mp3Data = mp3Data;
        song.setText(mp3Data.songtitle);
        artist.setText(mp3Data.artistname);
        if (!TextUtils.isEmpty(mp3Data.thumbnail)){
            thumbnailImage.setImageURI(Uri.fromFile(new File(mp3Data.thumbnail)));
        }
    }
}
