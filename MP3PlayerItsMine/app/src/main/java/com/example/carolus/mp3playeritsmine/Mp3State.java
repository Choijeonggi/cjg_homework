package com.example.carolus.mp3playeritsmine;

/**
 * Created by carolus on 2017-06-28.
 */

public class Mp3State {
    public static final int STATE_NONE = 20;
    public static final int STATE_PLAYING = 29;
    public static final int STATE_PAUSE = 30;
    public static final int STATE_PREPARING = 40;

    public static final int STATE_ENABLE_SHUFFLE=99;
    public static final int STATE_DISABLE_SHUFFLE=98;

    public static final int STATE_SONG_AGAIN=97;
    public static final int STATE_JUST_PLAY=96;

    public static final long FINISH_INTERVAL_TIME=2000;

    public static final String BUTTON_STATE_MP3LIST = "mp3List";
    public static final String BUTTON_STATE_PREVBTN = "prevBtn";
    public static final String BUTTON_STATE_PLAYPAUSEBTN= "playPauseBtn";
    public static final String BUTTON_STATE_NEXTBTN = "nextBtn";
}
