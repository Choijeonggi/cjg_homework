package com.example.carolus.mp3playeritsmine;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import static android.content.ContentValues.TAG;

/**
 * Created by carolus on 2017-06-27.
 */

public class Mp3Service extends Service {

    private static final String KEY_ACTION_PLAY_AND_PAUSE = "ACTION_PLAY_AND_PAUSE";
    private static final String KEY_ACTION_PREVIOUS = "PREVIOUS";
    private static final String KEY_ACTION_NEXT = "ACTION_NEXT";

    private static final int REQ_MAINACTIVITY = 23;
    private static final int REQ_PREVIOUS = 24;
    private static final int REQ_PLAYANDPAUSE = 25;
    private static final int REQ_NEXT = 26;

    private Mp3Binder mp3Binder = new Mp3Binder();
    private Mp3Adapter mp3Adapter = new Mp3Adapter();
    private MediaPlayer mediaPlayer = null;
    private Mp3Data currentMp3Data = null;

    private int state = Mp3State.STATE_NONE;
    private int newPosition;
    private int changeImage = android.R.drawable.ic_media_pause;

    private String changeString = "Pause";

    public String buttonState = Mp3State.BUTTON_STATE_MP3LIST;
    public int listPosition;
    public int shuffleState = Mp3State.STATE_DISABLE_SHUFFLE;
    public int playSongAgainState = Mp3State.STATE_JUST_PLAY;

    private ReentrantLock lock = new ReentrantLock();


    private IStateChange iStateChangeListener;

    private boolean isActivatedMediaPlayerWatcher =false;

    private boolean isActivatedThreadAndHandler=true;


    public boolean startTouchPlaybar = true;
    public int playbarMaxTime;
    public int playbarCurrentPosition;



    @Override
    public IBinder onBind(Intent intent) {



        return mp3Binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        refresh();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!TextUtils.isEmpty(intent.getAction())) {
            if (intent.getAction().equals(KEY_ACTION_PREVIOUS)) {
                previous();
            } else if (intent.getAction().equals(KEY_ACTION_PLAY_AND_PAUSE)) {
                playAndPause();
            } else if (intent.getAction().equals(KEY_ACTION_NEXT)) {
                next();
            }
        }

        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        isActivatedThreadAndHandler=false;
    }

    public ArrayList<Mp3Data> refresh() {
        ArrayList<Mp3Data> mp3Items = new ArrayList<>();
        mp3Items.clear();
        final Cursor cursor = getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null,
                null,
                null,
                null
        );

        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
            String songTitle = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
            String artistName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
            long albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));

            Mp3Data mp3Data = new Mp3Data();
            mp3Data.id = id;
            mp3Data.artistname = artistName;
            mp3Data.songtitle = songTitle;

            Cursor cursorThumbnail = getContentResolver().query(
                    MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Audio.Albums.ALBUM_ART},
                    MediaStore.Audio.Albums._ID + "=?",
                    new String[]{String.valueOf(albumId)},
                    null
            );

            if (cursorThumbnail.moveToNext()) {
                String thumbnailPath = cursorThumbnail.getString(cursorThumbnail.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
                if (!TextUtils.isEmpty(thumbnailPath)) {
                    mp3Data.thumbnail = thumbnailPath;
                }
            }
            mp3Items.add(mp3Data);
        }
        mp3Adapter.setMp3Items(mp3Items);
        return mp3Items;
    }


    class Mp3Binder extends Binder {
        Mp3Service getService() {
            return Mp3Service.this;
        }
    }

    //플레이관련은 한곳에서만 관리
    public void playAgain() {
        changeImage = android.R.drawable.ic_media_pause;
        changeString = "Pause";
        loadMusic(mp3Adapter.getItem(listPosition));
        Log.e(TAG, "songagain  " + listPosition);
        showNotification(currentMp3Data);
        state = Mp3State.STATE_PLAYING;
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playAgain();
            }
        });
    }

    public void playByMp3ListClicked() {
        Log.e(TAG, "playbyclicked");
        changeImage = android.R.drawable.ic_media_pause;
        changeString = "Pause";
        loadMusic(mp3Adapter.getItem(listPosition));
        showNotification(currentMp3Data);
        state = Mp3State.STATE_PLAYING;
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                next();
            }
        });
        if (iStateChangeListener != null) {
            iStateChangeListener.onStateChanged(state);
        }
    }

    public void playAndPause() {
        switch (state) {
            case Mp3State.STATE_NONE:
                changeImage = android.R.drawable.ic_media_pause;
                changeString = "Pause";
                loadMusic(mp3Adapter.getItem(0));
                showNotification(currentMp3Data);
                state = Mp3State.STATE_PLAYING;
                buttonState = Mp3State.BUTTON_STATE_PLAYPAUSEBTN;
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        next();
                    }
                });
                break;
            case Mp3State.STATE_PLAYING:
                Log.e(TAG, "clicked pause");
                changeString = "Play";
                changeImage = android.R.drawable.ic_media_play;
                showNotification(currentMp3Data);
                mediaPlayer.pause();
                state = Mp3State.STATE_PAUSE;
                break;
            case Mp3State.STATE_PAUSE:
                Log.e(TAG, "clicked play");
                changeImage = android.R.drawable.ic_media_pause;
                changeString = "Pause";
                showNotification(currentMp3Data);
                mediaPlayer.start();
                state = Mp3State.STATE_PLAYING;
                break;
        }
        if (iStateChangeListener != null) {
            iStateChangeListener.onStateChanged(state);
        }
    }

    public void previous() {
        switch (state) {
            case Mp3State.STATE_NONE:
                changeImage = android.R.drawable.ic_media_pause;
                changeString = "Pause";
                loadMusic(mp3Adapter.getItem(mp3Adapter.getCount() - 1));
                showNotification(currentMp3Data);
                state = Mp3State.STATE_PLAYING;
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        previous();
                    }
                });
                break;
            case Mp3State.STATE_PLAYING:
            case Mp3State.STATE_PAUSE:
                changeImage = android.R.drawable.ic_media_pause;
                changeString = "Pause";
                showNotification(currentMp3Data);
                int currentPosition = mp3Adapter.getMp3Items().indexOf(currentMp3Data);
                newPosition = currentPosition - 1;
                Log.e(TAG, "prev 1 :  " + newPosition);
                if (newPosition < 0) {
                    newPosition = mp3Adapter.getCount() - 1;
                }
                loadMusic(mp3Adapter.getItem(newPosition));
                state = Mp3State.STATE_PLAYING;
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        previous();
                    }
                });
                break;
        }
        if (iStateChangeListener != null) {
            iStateChangeListener.onStateChanged(state);
        }
    }

    public void next() {
        switch (state) {
            case Mp3State.STATE_NONE:
                changeImage = android.R.drawable.ic_media_pause;
                changeString = "Pause";
                loadMusic(mp3Adapter.getItem(0));
                showNotification(currentMp3Data);
                state = Mp3State.STATE_PLAYING;
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        next();
                    }
                });
                break;
            case Mp3State.STATE_PLAYING:
            case Mp3State.STATE_PAUSE:
                changeImage = android.R.drawable.ic_media_pause;
                changeString = "Pause";
                showNotification(currentMp3Data);
                int currentPosition = mp3Adapter.getMp3Items().indexOf(currentMp3Data);
                newPosition = currentPosition + 1;
                Log.e(TAG, "next 1 :  " + newPosition);
                if (newPosition >= mp3Adapter.getCount()) {
                    newPosition = 0;
                }
                loadMusic(mp3Adapter.getItem(newPosition));
                state = Mp3State.STATE_PLAYING;
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        next();
                    }
                });
                break;
        }
        if (iStateChangeListener != null) {
            iStateChangeListener.onStateChanged(state);
        }
    }

    public void enableRandom() {
        Log.e(TAG, "enablerandom");
        if (state == Mp3State.STATE_NONE) {
            playByShuffle();
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playByShuffle();
            }
        });
    }

    public void disableRandom() {
        Log.e(TAG, "disablerandom");
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                switch (buttonState) {
                    case Mp3State.BUTTON_STATE_PREVBTN:
                        Log.e(TAG, "random prev");
                        previous();
                        break;
                    case Mp3State.BUTTON_STATE_MP3LIST:
                    case Mp3State.BUTTON_STATE_PLAYPAUSEBTN:
                    case Mp3State.BUTTON_STATE_NEXTBTN:
                        Log.e(TAG, "random next, random mp3List, random playpause");
                        next();
                        break;
                }
            }
        });
    }

    public void playByShuffle() {
        switch (state) {
            case Mp3State.STATE_NONE:
                changeImage = android.R.drawable.ic_media_pause;
                changeString = "Pause";
                newPosition = shuffle();
                Log.e(TAG, "random 1 :  " + newPosition);
                loadMusic(mp3Adapter.getItem(newPosition));
                showNotification(currentMp3Data);
                state = Mp3State.STATE_PLAYING;
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        playByShuffle();
                    }
                });
                break;
            case Mp3State.STATE_PLAYING:
            case Mp3State.STATE_PAUSE:
                changeImage = android.R.drawable.ic_media_pause;
                changeString = "Pause";
                showNotification(currentMp3Data);
                newPosition = shuffle();
                Log.e(TAG, "random 1 :  " + newPosition);
                loadMusic(mp3Adapter.getItem(newPosition));
                state = Mp3State.STATE_PLAYING;
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        playByShuffle();
                    }
                });
                break;
        }
    }

    public int shuffle() {
        Random random = new Random();
        int randomPlayList = random.nextInt(mp3Adapter.getCount() - 1);
        return randomPlayList;
    }

    public void loadMusic(Mp3Data mp3Data) {
        if (currentMp3Data != null) {
            if (currentMp3Data.equals(mp3Data)) {
                Log.e(TAG, "list second clicked");
                return;
            }
        }
//        if (startTouchPlaybar==false){
//            startTouchPlaybar=true;
//            mediaPlayer.seekTo(playbarCurrentPosition);
//        }
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }

        mediaPlayer = new MediaPlayer();

        try {
            Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, mp3Data.id);

            mediaPlayer.setDataSource(Mp3Service.this, uri);
            state = Mp3State.STATE_PREPARING;
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                    if (isActivatedMediaPlayerWatcher ==false){
                        isActivatedMediaPlayerWatcher =true;
                        mediaPlayerWatcher.start();
                    }
                    state = Mp3State.STATE_PLAYING;
                }
            });

            showNotification(mp3Data);

        } catch (IOException e) {
            currentMp3Data = null;
            mediaPlayer = null;
            state = Mp3State.STATE_NONE;
            e.printStackTrace();
        }
        currentMp3Data = mp3Data;
        Log.e(TAG, "load music first");


        if (currentMp3Data != null) {
            iStateChangeListener.onMp3DataChanged(mp3Data);
            iStateChangeListener.playbarStateChanged(mediaPlayer.getDuration());
        }

    }

    public void showNotification(Mp3Data mp3Data) {

        Intent prevIntent = new Intent(Mp3Service.this, Mp3Service.class);
        prevIntent.setAction(KEY_ACTION_PREVIOUS);

        Intent playAndPauseIntent = new Intent(Mp3Service.this, Mp3Service.class);
        playAndPauseIntent.setAction(KEY_ACTION_PLAY_AND_PAUSE);

        Intent nextIntent = new Intent(Mp3Service.this, Mp3Service.class);
        nextIntent.setAction(KEY_ACTION_NEXT);

        Bitmap icon = getResizedAlbumImage(mp3Data.thumbnail);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(mp3Data.songtitle)
                .setContentText(mp3Data.artistname)
                .setSmallIcon(R.drawable.default_icon)
                .setLargeIcon(icon)
                .setContentIntent(
                        PendingIntent.getActivity(Mp3Service.this, REQ_MAINACTIVITY,
                                new Intent(Mp3Service.this, MainActivity.class),
                                PendingIntent.FLAG_UPDATE_CURRENT)
                )
                .addAction(android.R.drawable.ic_media_previous, "Prev",
                        PendingIntent.getService(
                                Mp3Service.this,
                                REQ_PREVIOUS,
                                prevIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        )
                )
                .addAction(changeImage, changeString,
                        PendingIntent.getService(
                                Mp3Service.this,
                                REQ_PLAYANDPAUSE,
                                playAndPauseIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        )
                )
                .addAction(android.R.drawable.ic_media_next, "Next",
                        PendingIntent.getService(
                                Mp3Service.this,
                                REQ_NEXT,
                                nextIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        )
                )
                .build();
        startForeground(1, notification);
        if (icon != null) {
            icon.recycle();
        }

    }

    public Bitmap getResizedAlbumImage(String path) {

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, option);
        int thumbnailWidth = option.outWidth;
        int thumbnailHeight = option.outHeight;

        int scaleFactor = Math.min(thumbnailWidth / (int) (64f * displayMetrics.density), thumbnailHeight / (int) (64f * displayMetrics.density));

        option.inJustDecodeBounds = false;
        option.inSampleSize = scaleFactor;
        option.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path, option);
        return bitmap;
    }

    public void tossedProgress(int progress) {
        playbarCurrentPosition = progress;
        startTouchPlaybar = true;
        mediaPlayer.seekTo(playbarCurrentPosition);
    }


    Thread mediaPlayerWatcher = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    if (!lock.isLocked()) {
                        int nowTime = mediaPlayer.getCurrentPosition();
                        handler.sendEmptyMessage(nowTime);
                    }
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    });
    android.os.Handler handler = new android.os.Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int nowTime = msg.what;
            if (isActivatedThreadAndHandler==true) {
                iStateChangeListener.handlerValueChanged(nowTime);
            }
        }
    };


    public Mp3Adapter getMp3Adapter() {
        return mp3Adapter;
    }

    public int getState() {
        return state;
    }

    public void setOnStateChangedListener(IStateChange listener) {
        iStateChangeListener = listener;
    }

    public Mp3Data getCurrentMp3Data() {
        return currentMp3Data;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public ReentrantLock getLock() {
        return lock;
    }
}
