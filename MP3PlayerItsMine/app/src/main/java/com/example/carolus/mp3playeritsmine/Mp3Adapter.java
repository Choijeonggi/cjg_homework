package com.example.carolus.mp3playeritsmine;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by carolus on 2017-06-27.
 */

public class Mp3Adapter extends BaseAdapter {

    private ArrayList<Mp3Data> mp3Items=new ArrayList<>();

    public void setMp3Items(ArrayList<Mp3Data> mp3Items) {
        this.mp3Items = mp3Items;
        notifyDataSetChanged();
    }

    public ArrayList<Mp3Data> getMp3Items() {
        return mp3Items;
    }

    @Override
    public int getCount() {
        return mp3Items.size();
    }

    @Override
    public Mp3Data getItem(int position) {
        return mp3Items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mp3Items.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView=new Mp3View(parent.getContext());
        }

        ((Mp3View)convertView).setMp3Data(mp3Items.get(position));


        return convertView;
    }
}
