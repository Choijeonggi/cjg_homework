package com.example.carolus.mp3playeritsmine;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by carolus on 2017-06-27.
 */

public class Mp3Data implements Parcelable{
    public long id;
    public String artistname;
    public String songtitle;
    public String thumbnail;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.artistname);
        dest.writeString(this.songtitle);
        dest.writeString(this.thumbnail);
    }

    public Mp3Data() {
    }

    protected Mp3Data(Parcel in) {
        this.id = in.readLong();
        this.artistname = in.readString();
        this.songtitle = in.readString();
        this.thumbnail = in.readString();
    }

    public static final Creator<Mp3Data> CREATOR = new Creator<Mp3Data>() {
        @Override
        public Mp3Data createFromParcel(Parcel source) {
            return new Mp3Data(source);
        }

        @Override
        public Mp3Data[] newArray(int size) {
            return new Mp3Data[size];
        }
    };
}
