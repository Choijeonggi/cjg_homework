package com.example;

/**
 * Created by carolus on 2017-07-02.
 */

public interface ILackFuel {
    void onLackFuel();
}
