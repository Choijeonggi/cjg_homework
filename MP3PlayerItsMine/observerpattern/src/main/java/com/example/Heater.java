package com.example;

/**
 * Created by carolus on 2017-07-02.
 */

public class Heater {

    float fuel = 0.0f;



    ILackFuel listener = null;


    public void setfuel(float fuel){
        this.fuel = fuel;
    }

    public void heat(){
        fuel-=1;
        if(fuel<0){
            fuel = 0;
            if(listener!=null){
                listener.onLackFuel();
            }

        }
    }

    public void showFuel(){
        System.out.println("fuel:"+fuel);
    }

    public void setOnLackFuelListener(ILackFuel listener){
        this.listener = listener;
    }

}
