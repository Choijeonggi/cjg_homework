package com.example;

/**
 * Created by carolus on 2017-07-02.
 */

public class Car {
    //프로퍼티, Car클래스멤버변수
    protected String name;
    protected float velocity = 0.0f;
    float maxVelocity;
    float fuel;


    //기본생성자,명시하지_않아도_기본적으로_존재
   Car(){

   }

    //메소드=액션,기능추가

    void accelate(){
        System.out.println(name+" is acelating");

        if (velocity +10.0f > maxVelocity){
            velocity = maxVelocity;
            System.out.println(name+" is already full speed");
        }else{
            velocity += 10.0f;
        }
    }

    void showName(){
        System.out.println("Name:"+name);
    }

    void showVelocity(){
        System.out.println(name+" velocity:"+ velocity+"km/h");
    }
}
