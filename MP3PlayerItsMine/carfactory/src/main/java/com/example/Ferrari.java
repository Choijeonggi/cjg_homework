package com.example;

/**
 * Created by carolus on 2017-07-02.
 */

public class Ferrari extends Car{

    public Ferrari() {
        name = "Ferrari";
        maxVelocity = 220.0f;
        fuel = 80.0f;
    }

    @Override
    void accelate() {
        fuel -= 3.0f;
        if(fuel<0){
            fuel = 0.0f;
            velocity = 0.0f;
            System.out.println("lack fuel");
            return;
        }
        super.accelate();
    }

    public void openRoof(){
        System.out.print("ferrari open roof");
    }
}
