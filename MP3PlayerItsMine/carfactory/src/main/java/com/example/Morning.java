package com.example;

/**
 * Created by carolus on 2017-07-02.
 */

public class Morning extends Car{

    Morning() {
        super();
        maxVelocity = 100.0f;
        name = "morning";
        fuel = 50.0f;
    }

    @Override
    void accelate() {
        fuel -= 1.0f;
        if(fuel<0){
            fuel = 0.0f;
            velocity = 0.0f;
            return;
        }
        super.accelate();
    }


}
