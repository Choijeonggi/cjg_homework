package com.example.carolus.mylistviewpractice1;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by carolus on 2017-05-27.
 */

public class PracticeAdapter extends BaseAdapter {

    private ArrayList<PracticeData> items = new ArrayList<>();

    public void setItems(ArrayList<PracticeData> items){
        this.items=items;
        notifyDataSetChanged();

    }

    public void add(PracticeData data){


       items.add(data);
        notifyDataSetChanged();
    }



    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null){
            convertView=new PracticeView(parent.getContext());
        }


        ((PracticeView)convertView).setData(items.get(position));




        return convertView;
    }
}
