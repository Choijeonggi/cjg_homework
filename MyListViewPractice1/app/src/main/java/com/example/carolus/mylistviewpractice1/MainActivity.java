package com.example.carolus.mylistviewpractice1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView MainList;
    EditText WriteText;
    Button AddButton;
    PracticeAdapter adapter;


    String[] data = new String[]{"1","2"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainList= (ListView) findViewById(R.id.MainList);
        WriteText= (EditText) findViewById(R.id.WriteText);
        AddButton= (Button) findViewById(R.id.AddButton);

        final ArrayList<PracticeData> items = new ArrayList<>();

        for (int i = 0;i<data.length;i++){
            PracticeData practiceData = new PracticeData();
            practiceData.title=data[i];
            items.add(practiceData);
        }



        adapter = new PracticeAdapter();
        MainList.setAdapter(adapter);
        adapter.setItems(items);



        AddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                adapter.add(WriteText.getText().toString());
            }
        });

    }




}
