package com.example.carolus.mylistviewpractice1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by carolus on 2017-05-27.
 */

public class PracticeView extends FrameLayout{
    private TextView viewTitle;
    private Context context;

    private PracticeData data;



    public PracticeView(@NonNull Context context) {
        super(context);
        this.context=context;
        View view = LayoutInflater.from(context).inflate(R.layout.practice_view,this,true);

        viewTitle= (TextView) view.findViewById(R.id.viewtTitle);

    }


public void setData(PracticeData data){
    this.data=data;
    viewTitle.setText(data.title);
}



}



