package com.carolus.memoapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.LayoutInflaterCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import javax.xml.transform.Source;

/**
 * Created by carolus on 2017-04-22.
 */

public class SourceView extends FrameLayout {

    private TextView timeView;
    private TextView titleView;
    private SourceData data;
    private Context context;

    public SourceView(@NonNull Context context) {
        super(context);
        this.context =context;
        View view = LayoutInflater.from(context).inflate(R.layout.list_source,this,true);

        timeView = (TextView) view.findViewById(R.id.memoTime);
        titleView = (TextView) view.findViewById(R.id.title);

    }

    public void setData(SourceData data){
        this.data=data;
        titleView.setText(data.memotitle);
        timeView.setText(data.writetime);

    }
}
