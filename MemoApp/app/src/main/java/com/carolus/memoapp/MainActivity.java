package com.carolus.memoapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    String[] data = new String[]{"q","w","e","r","t","y","u","i"};

    String[] datatime = new String[]{"1","2","3","4","5","6","7","8","9","0"};

    ListView memoList;
    MemoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<SourceData> sources = new ArrayList<>();

        for (int i=0; i<data.length; i++){

            SourceData sourceData = new SourceData();
            sourceData.id = i;
            sourceData.memotitle = data[i];
          //  sourceData.writetime = datatime[i];
            sources.add(sourceData);

        }






        memoList = (ListView) findViewById(R.id.memoList);

        adapter = new MemoAdapter();

        memoList.setAdapter(adapter);
        adapter.setSourceDatas(sources);

        memoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, ((SourceData)adapter.getItem(position)).memotitle, Toast.LENGTH_SHORT).show();
            }
        });



    }

    private static final int MENU_DEFAULT_VALUE = 0;
    private static final int MENU_MOVE_WRITE_PAGE =0;

    public boolean onCreateOptionMenu(Menu menu){
        MenuItem moveWritePage = menu.add(MENU_DEFAULT_VALUE, MENU_MOVE_WRITE_PAGE, MENU_MOVE_WRITE_PAGE,"write");
        moveWritePage.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
}
