package com.carolus.memoapp;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by carolus on 2017-04-22.
 */

public class MemoAdapter extends BaseAdapter {

    private ArrayList<SourceData> sourceDatas = new ArrayList<>();

    public void setSourceDatas(ArrayList<SourceData> sourceDatas){
        this.sourceDatas = sourceDatas;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return sourceDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return sourceDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return sourceDatas.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView==null){
            convertView = new SourceView(parent.getContext());

        }

        ((SourceView)convertView).setData(sourceDatas.get(position));

        return convertView;
    }
}
