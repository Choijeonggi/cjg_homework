package com.carolus.pushtest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;

public class MainActivity extends AppCompatActivity {

    TextView text;
    ImageView image;

    private static final String TAG = "tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (TextView) findViewById(R.id.text);
        image = (ImageView) findViewById(R.id.image);


        String token = FirebaseInstanceId.getInstance().getToken();

        Log.e(TAG, "token: " + token);

        setContent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setContent(intent);
    }

    private void setContent(Intent intent) {
        if (intent.hasExtra("phonenum") && intent.hasExtra("image")) {
            String phoneNum = getIntent().getStringExtra("phonenum");
            String image = getIntent().getStringExtra("image");
            text.setText("number:" + phoneNum);
            Glide.with(this).load(image).into(this.image);

        } else {
            text.setText("no data");
        }

    }
}
