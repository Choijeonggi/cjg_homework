package com.example.carolus.thisismemo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by carolus on 2017-07-28.
 */

public class MemoView extends FrameLayout {

    TextView titleInMainList;

    public MemoView(@NonNull Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.memo_view, this, true);

        titleInMainList = view.findViewById(R.id.titleInMainList);
    }

    public void setMemoData(MemoData memoData) {
        titleInMainList.setText(memoData.getMemoTitle());
    }
}
