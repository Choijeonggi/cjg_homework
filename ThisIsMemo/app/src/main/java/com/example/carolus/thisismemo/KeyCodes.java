package com.example.carolus.thisismemo;

/**
 * Created by carolus on 2017-07-29.
 */

public class KeyCodes {

    public static final int KEY_REQ_REWRITE_MEMO = 0;
    public static final int KEY_REQ_NEWWRITE_MEMO = 1;

    public static final String KEY_POSITION = "POSITION";
    public static final String KEY_REWRITE = "REWRITE";
    public static final String TAG = "MAINACTIVITY";
}
