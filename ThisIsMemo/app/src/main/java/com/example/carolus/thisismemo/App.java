package com.example.carolus.thisismemo;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by carolus on 2017-07-28.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
    }
}
