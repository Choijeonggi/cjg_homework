package com.example.carolus.thisismemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView mainMemoList;

    MemoAdapter memoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainMemoList = (ListView) findViewById(R.id.mainMemoList);

        memoAdapter = new MemoAdapter();
        mainMemoList.setAdapter(memoAdapter);

        mainMemoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MemoData memoData = memoAdapter.getItem(i);

                Intent reWriteIntent = new Intent(MainActivity.this, MemoWriteActivity.class);
                reWriteIntent.putExtra(KeyCodes.KEY_REWRITE, memoData);
                reWriteIntent.putExtra(KeyCodes.KEY_POSITION, i);

                startActivityForResult(reWriteIntent, KeyCodes.KEY_REQ_REWRITE_MEMO);
                Log.e(KeyCodes.TAG, "memodata: " + memoData + "\nposition: " + i);
            }
        });

        mainMemoList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                memoAdapter.deleteMemo(i);
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem write = menu.add(0, 0, 0, "Write");
        write.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Intent newWriteIntent = new Intent(MainActivity.this, MemoWriteActivity.class);
                startActivityForResult(newWriteIntent, KeyCodes.KEY_REQ_NEWWRITE_MEMO);
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KeyCodes.KEY_REQ_REWRITE_MEMO && resultCode == RESULT_OK) {
            int listPosition = data.getIntExtra(KeyCodes.KEY_POSITION, -1);
            Log.e(KeyCodes.TAG, "listposition: " + listPosition);

            memoAdapter.deleteMemo(listPosition);
            Log.e(KeyCodes.TAG, "delete");
        }
    }
}
