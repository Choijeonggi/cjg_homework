package com.example.carolus.thisismemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import io.realm.Realm;

/**
 * Created by carolus on 2017-07-28.
 */

public class MemoWriteActivity extends AppCompatActivity {

    EditText writeActTitle, writeActContent;

    private int position;

    private String savedTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memo_write_activity);

        writeActTitle = (EditText) findViewById(R.id.writeActTitle);
        writeActContent = (EditText) findViewById(R.id.writeActContent);

        if (getIntent().hasExtra(KeyCodes.KEY_REWRITE)) {
            MemoData memoData = getIntent().getParcelableExtra(KeyCodes.KEY_REWRITE);

            writeActTitle.setText(memoData.getMemoTitle());
            writeActContent.setText(memoData.getMemoContent());
            savedTitle = memoData.getMemoTitle();
            position = getIntent().getIntExtra(KeyCodes.KEY_POSITION, -1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem back = menu.add(0, 0, 0, "Back");
        back.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Realm realm = Realm.getDefaultInstance();

        String strTitle = writeActTitle.getText().toString();
        String strContent = writeActContent.getText().toString();

        if (!strTitle.equals(savedTitle)) {
            Intent intent = new Intent(MemoWriteActivity.this, MainActivity.class);
            intent.putExtra(KeyCodes.KEY_POSITION, position);
            setResult(RESULT_OK, intent);
        }

        MemoData memoData = new MemoData(strTitle, strContent);

        Log.e(KeyCodes.TAG, "can delete");

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(memoData);
        realm.commitTransaction();

        finish();
        return true;
    }


}
