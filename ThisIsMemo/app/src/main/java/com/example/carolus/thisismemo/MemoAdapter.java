package com.example.carolus.thisismemo;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by carolus on 2017-07-28.
 */

public class MemoAdapter extends BaseAdapter {

    private RealmResults<MemoData> memoDatas;
    private Realm realm;

    private RealmChangeListener<RealmResults<MemoData>> realmChangeListener = new RealmChangeListener<RealmResults<MemoData>>() {
        @Override
        public void onChange(RealmResults<MemoData> element) {
            memoDatas = element;
            notifyDataSetChanged();
        }
    };

    public MemoAdapter() {
        realm = Realm.getDefaultInstance();
        memoDatas = realm.where(MemoData.class).findAll();
        memoDatas.addChangeListener(realmChangeListener);
    }

    public void deleteMemo(int position) {
        realm.beginTransaction();
        memoDatas.get(position).deleteFromRealm();
        realm.commitTransaction();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return memoDatas.size();
    }

    @Override
    public MemoData getItem(int i) {
        return memoDatas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MemoView memoView = (MemoView) view;
        if (memoView == null) {
            memoView = new MemoView(viewGroup.getContext());
        }
        memoView.setMemoData(memoDatas.get(i));
        return memoView;
    }
}
