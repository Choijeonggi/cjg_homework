package com.example.carolus.thisismemo;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by carolus on 2017-07-28.
 */

public class MemoData extends RealmObject implements Parcelable {

    @PrimaryKey
    private String memoTitle;
    private String memoContent;

    public MemoData() {

    }

    public MemoData(String memoTitle, String memoContent) {
        this.memoTitle = memoTitle;
        this.memoContent = memoContent;
    }

    public String getMemoTitle() {
        return memoTitle;
    }

    public String getMemoContent() {
        return memoContent;
    }

    protected MemoData(Parcel in) {
        memoTitle = in.readString();
        memoContent = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(memoTitle);
        dest.writeString(memoContent);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MemoData> CREATOR = new Creator<MemoData>() {
        @Override
        public MemoData createFromParcel(Parcel in) {
            return new MemoData(in);
        }

        @Override
        public MemoData[] newArray(int size) {
            return new MemoData[size];
        }
    };
}
