package com.example.carolus.myprctslistview2;

import android.view.*;
import android.view.View;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by carolus on 2017-05-27.
 */

public class Adapter extends BaseAdapter {

    private ArrayList<Data> items = new ArrayList<>();

    public void setItems(ArrayList<Data> items) {
        this.items = items;
        notifyDataSetChanged();
    }



    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView=new View(parent.getContext());
        }

        ((prctsView)convertView).setData(items.get(position));


        return convertView;
    }
}
