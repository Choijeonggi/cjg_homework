package com.example.carolus.myprctslistview2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.*;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    String[] data = new String[]{"1","2","3","4"};

    ListView listview;

    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview= (ListView) findViewById(R.id.listview);

        final ArrayList<Data> items = new ArrayList<>();

        for (int i = 0 ; i<data.length;i++){
            Data sourceData = new Data();
            sourceData.string=data[i];
            items.add(sourceData);
        }

        adapter=new Adapter();
        listview.setAdapter(adapter);
        adapter.setItems(items);


    }
}
