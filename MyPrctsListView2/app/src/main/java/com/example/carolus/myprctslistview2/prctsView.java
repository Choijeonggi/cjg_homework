package com.example.carolus.myprctslistview2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by carolus on 2017-05-27.
 */

public class prctsView extends FrameLayout {

    private TextView viewsource;
    private Context context;
    private Data data;


    public prctsView(@NonNull Context context) {
        super(context);
        this.context=context;
        View view = LayoutInflater.from(context).inflate(R.layout.view_layout,this,true);
        viewsource= (TextView) view.findViewById(R.id.viewsource);


    }


    public void setData(Data data) {
        this.data = data;
        viewsource.setText(data.string);
    }
}
