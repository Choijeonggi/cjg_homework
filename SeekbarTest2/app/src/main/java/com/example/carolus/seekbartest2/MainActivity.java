package com.example.carolus.seekbartest2;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    SeekBar seekBar;
    TextView textView;
MediaPlayer mediaPlayer;
    Button playButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar= (SeekBar) findViewById(R.id.Seekbar);
        textView= (TextView) findViewById(R.id.text);
        playButton= (Button) findViewById(R.id.playButton);

        mediaPlayer.setDataSource(R.raw.test);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mediaPlayer.start();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView.setText("onProgressChanged : "+progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                textView.setText("onStart TracingTouch");

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textView.setText("onStop Tracking Touch");

            }
        });
    }
}
