package com.example.carolus.mp3practice1;

/**
 * Created by carolus on 2017-05-30.
 */

public class MP3Data {
    public int id;
    public String artistname;
    public String songtitle;
    public String thumbnail;
}
