package com.example.carolus.mp3practice1;

import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ListView;

import java.util.ArrayList;

public class MP3MainActivity extends AppCompatActivity {


    ListView MP3list;
    MP3Adapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MP3list= (ListView) findViewById(R.id.MP3List);


        ArrayList<MP3Data> MP3items=new ArrayList<>();


        Cursor cursor =getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null,null,null,null
        );



        while (cursor.moveToNext()){
            String songname=cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
            String artistname=cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

            long albumid = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));

            MP3Data mp3Data = new MP3Data();

            Cursor thumbnailcursor = getContentResolver().query(
                    MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Audio.Albums.ALBUM_ART},
                    MediaStore.Audio.Albums._ID+"=?",
                    new String[]{String.valueOf(albumid)},
                    null
            );
            if (thumbnailcursor.moveToNext()){
                String thumbnailpath = thumbnailcursor.getString(thumbnailcursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
                if (!TextUtils.isEmpty(thumbnailpath)){
                    mp3Data.thumbnail=thumbnailpath;
                }
            }



//            MP3Data mp3Data = new MP3Data();
            mp3Data.artistname="song  "+songname;
            mp3Data.songtitle="artist  "+artistname;
            MP3items.add(mp3Data);
        }


        adapter=new MP3Adapter();
        MP3list.setAdapter(adapter);
        adapter.setItems(MP3items);


    }
}
