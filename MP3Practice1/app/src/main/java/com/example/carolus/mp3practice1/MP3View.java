package com.example.carolus.mp3practice1;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

/**
 * Created by carolus on 2017-05-30.
 */

public class MP3View extends FrameLayout {


    private ImageView thumbnail;
    private TextView songname,artistname;
    private Context context;
    private MP3Data data;


    public MP3View(@NonNull Context context) {
        super(context);
        this.context=context;
        View view= LayoutInflater.from(context).inflate(R.layout.mp3_view,this,true);
        thumbnail= (ImageView) view.findViewById(R.id.thumbnail_view);
        songname= (TextView) view.findViewById(R.id.songname_view);
        artistname= (TextView) view.findViewById(R.id.artistname_view);

    }


    public void setData(MP3Data data) {
        this.data = data;
        //thumbnail.setImageResource(data.thumbnail);
        songname.setText(data.songtitle);
        artistname.setText(data.artistname);
        if(!TextUtils.isEmpty(data.thumbnail)){
            thumbnail.setImageURI(Uri.fromFile(new File(data.thumbnail)));
        }
    }
}
