package com.example.carolus.mp3practice1;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by carolus on 2017-05-30.
 */

public class MP3Adapter extends BaseAdapter {

    private ArrayList<MP3Data> items = new ArrayList<>();

    public void setItems(ArrayList<MP3Data> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView=new MP3View(parent.getContext());

        }

        ((MP3View)convertView).setData(items.get(position));
        return convertView;
    }
}
