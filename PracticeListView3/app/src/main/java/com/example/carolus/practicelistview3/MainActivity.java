package com.example.carolus.practicelistview3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView ThisIsList;
    ListViewAdapter adapter;


    String[] data = new String[]{"5","3","2","1"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ThisIsList= (ListView) findViewById(R.id.thisIsList);


        final ArrayList<ListViewData> items = new ArrayList<>();

        for (int i=0;i<data.length;i++){
            ListViewData listViewData = new ListViewData();
            listViewData.string=data[i];
            items.add(listViewData);
        }

        adapter=new ListViewAdapter();
        ThisIsList.setAdapter(adapter);
        adapter.setItems(items);


    }
}
