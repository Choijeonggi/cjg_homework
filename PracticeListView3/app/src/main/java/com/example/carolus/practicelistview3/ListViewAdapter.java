package com.example.carolus.practicelistview3;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by carolus on 2017-05-27.
 */

public class ListViewAdapter extends BaseAdapter {

    private ArrayList<ListViewData> items = new ArrayList<>();




    public void setItems(ArrayList<ListViewData> items) {
        this.items = items;
        notifyDataSetChanged();
    }



    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView==null){
            convertView=new ListViewView(parent.getContext());
        }


        ((ListViewView)convertView).setData(items.get(position));


        return convertView;

    }
}
