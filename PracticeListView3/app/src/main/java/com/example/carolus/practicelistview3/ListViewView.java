package com.example.carolus.practicelistview3;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by carolus on 2017-05-27.
 */

public class ListViewView extends FrameLayout {


    private TextView ThisIsView;
    private Context context;
    private ListViewData data;

    public ListViewView(@NonNull Context context) {
        super(context);
        this.context=context;
        View view = LayoutInflater.from(context).inflate(R.layout.view,this,true);
        ThisIsView= (TextView) view.findViewById(R.id.thisIsView);



    }

    public void setData(ListViewData data) {
        this.data = data;
        ThisIsView.setText(data.string);
    }
}
