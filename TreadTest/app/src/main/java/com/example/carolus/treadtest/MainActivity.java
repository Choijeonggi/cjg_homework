package com.example.carolus.treadtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    private Button start;
    private TextView count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        start= (Button) findViewById(R.id.start);
        count= (TextView) findViewById(R.id.count);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread1 = new Thread(new Runnable() {
                    int number = 0;
                    @Override
                    public void run() {
                        for (int i = 0; i < 1000000; i++) {
                            Log.e(TAG,"count: "+ i);
                            number++;
                        //액티비티에서만 할 수 있는 방법 자식스레드가 메인쓰레드에게 이벤트 전달하기
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                count.setText(String.valueOf(number));
                            }
                        });





                        }
                    }
                });
                thread1.start();
            }
        });
    }
}
