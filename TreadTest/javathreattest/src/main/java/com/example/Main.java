package com.example;

import java.util.concurrent.Semaphore;

public class Main {

    private static int count = 0;

    private static Object lock = new Object();

    private static Semaphore semaphore = new Semaphore(1);

    public static void main(String[] args) {
        System.out.println("start");

//        Thread thread1 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i < 10000000; i++) {
//                    synchronized (lock) {
//                        count++;
//                    }
//                }
//            }
//        });
//
//        Thread thread2 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i < 10000000; i++) {
//                    synchronized (lock) {
//                        count--;
//                    }
//                }
//
//            }
//        });

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000000; i++) {
                    try {
                        semaphore.acquire();
                        count++;
                        semaphore.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000000; i++) {
                    try {
                        semaphore.acquire();
                        count--;
                        semaphore.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

            }
        });


//            System.out.println("waiting for thread1,thread2");
        thread1.start();
        thread2.start();
//            System.out.println("finished to wait for thread1,thread2");

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("count :  " + count);
        System.out.println("finished");

    }
}
