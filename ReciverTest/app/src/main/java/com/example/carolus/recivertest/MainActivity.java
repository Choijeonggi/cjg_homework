package com.example.carolus.recivertest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    BroadcastReceiver localReceiver;
    Button register,unregister;

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        register = (Button) findViewById(R.id.register);
        unregister = (Button) findViewById(R.id.unregister);



        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(MainActivity.this, "register", Toast.LENGTH_SHORT).show();


                localReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        switch (intent.getAction()){
                            case Intent.ACTION_AIRPLANE_MODE_CHANGED:
                                Log.e(TAG, "onReceive: ACTION_AIRPLANE_MODE_CHANGED");
                                break;
                            case Intent.ACTION_BATTERY_CHANGED:
                                Log.e(TAG, "onReceive: ACTION_BATTERY_CHANGED");
                                break;
                        }
                    }
                };

                IntentFilter filter = new IntentFilter();
                filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
                filter.addAction(Intent.ACTION_BATTERY_CHANGED);
                registerReceiver(localReceiver,filter);
            }
        });

        unregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(MainActivity.this, "unregister", Toast.LENGTH_SHORT).show();

                if (localReceiver!=null){
                    unregisterReceiver(localReceiver);
                    localReceiver=null;
                }
            }
        });

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        localReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//
//
//
//
//
//
//
//
//
//            }
//        };
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//    }
}
