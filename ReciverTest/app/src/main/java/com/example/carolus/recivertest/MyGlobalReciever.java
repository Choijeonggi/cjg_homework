package com.example.carolus.recivertest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by carolus on 2017-04-30.
 */

public class MyGlobalReciever extends BroadcastReceiver {

    public static final String TAG = MyGlobalReciever.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        switch (action){
            case Intent.ACTION_BATTERY_OKAY:
                Log.e(TAG, "onReceive: ACTION_BATTERY_OKAY");
                break;
            case Intent.ACTION_BATTERY_LOW:
                Log.e(TAG, "onReceive: ACTION_BATTERY_LOW");
                break;
            case Intent.ACTION_BOOT_COMPLETED:
                Log.e(TAG, "onReceive: ACTION_BOOT_COMPLETED");

        }
    }
}
