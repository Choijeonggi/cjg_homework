package com.example.carolus.listviewtest;

/**
 * Created by carolus on 2017-05-28.
 */

public class ListData {
    public int id;
    public int icon;
    public int thumbnail1;
    public String songname;
    public String artist;
    public String thumbnail;
}
