package com.example.carolus.listviewtest;

import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


public static final String TAG = MainActivity.class.getSimpleName();
    ListView listview;
    ListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview= (ListView) findViewById(R.id.listview);

//        final ArrayList<ListData> items = new ArrayList<>();
//


        ArrayList<ListData> MP3items = new ArrayList<>();





        Cursor cursor = getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null,
                null,
                null,
                null
        );






        while (cursor.moveToNext()){
            String songname =cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
            String artistname =cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
//            int thumbnail = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.))


            long albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
            Log.e(TAG, "albumid  " +albumId);


            Cursor cursorthumnail = getContentResolver().query(
                    MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Audio.Albums.ALBUM_ART},
                    MediaStore.Audio.Albums._ID,
                    new String[]{String.valueOf(albumId)},
                    null
            );
            if (cursorthumnail.moveToNext()){
                String thumbnailPath = cursorthumnail.getString(cursorthumnail.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
                if (!TextUtils.isEmpty(thumbnailPath)){
                    MP3items.thumbnail=thumbnailPath;
                }
            }


            ListData mp3items = new ListData();
            mp3items.songname="노래  "+songname;
            mp3items.artist="가수  "+artistname;
            MP3items.add(mp3items);

            Log.e(TAG, " songname  "+songname);
            Log.e(TAG, " artist  " +artistname);
            Log.e(TAG, "" );
        }







//        for (int i =0;i<MP3items.size();i++){
//            ListData listData = new ListData();
//            listData.id=i;
//            listData.icon=R.mipmap.ic_launcher;
//            listData.thumbnail1 ="thumbnail1"+i;
//            listData.songname ="songname"+i;
//            MP3items.add(listData);
//        }





        adapter=new ListAdapter();
        listview.setAdapter(adapter);
        adapter.setItems(MP3items);






    }



}
