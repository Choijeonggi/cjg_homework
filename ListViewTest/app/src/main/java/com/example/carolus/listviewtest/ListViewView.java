package com.example.carolus.listviewtest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by carolus on 2017-05-28.
 */

public class ListViewView extends FrameLayout {

    private ImageView imageView;
    private TextView title,contents;
    private Context context;
    private ListData data;



    public ListViewView(@NonNull Context context) {
        super(context);
        this.context=context;
        View view = LayoutInflater.from(context).inflate(R.layout.view,this,true);
        imageView= (ImageView) view.findViewById(R.id.imageview);
        title= (TextView) view.findViewById(R.id.title);
        contents= (TextView) view.findViewById(R.id.contents);

    }


    public void setData(ListData data) {
        this.data = data;
        imageView.setImageResource(data.thumbnail1);
        title.setText(data.artist);
        contents.setText(data.songname);

    }
}
