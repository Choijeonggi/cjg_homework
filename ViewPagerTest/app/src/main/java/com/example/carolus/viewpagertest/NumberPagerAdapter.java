package com.example.carolus.viewpagertest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by carolus on 2017-04-30.
 */

public class NumberPagerAdapter extends FragmentStatePagerAdapter {


    public NumberPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return NumberFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 10;
    }
}
