package com.example.carolus.viewpagertest;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {


   ViewPager pager;
    NumberPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       pager= (ViewPager) findViewById(R.id.pager);
        adapter = new NumberPagerAdapter(getSupportFragmentManager());

        pager.setAdapter(adapter);

    }
}
