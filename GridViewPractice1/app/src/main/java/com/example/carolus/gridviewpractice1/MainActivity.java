package com.example.carolus.gridviewpractice1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    GridView gridView;

    GridAdapter adapter;
    String[] data = new String[]{"0","9","8","7","6","5","4","3","2","1"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gridView= (GridView) findViewById(R.id.gridview);

        final ArrayList<GridData> items = new ArrayList<>();

        for (int i=0;i<data.length;i++){
            GridData gridData = new GridData();
            gridData.string=data[i];
            items.add(gridData);
        }

        adapter=new GridAdapter();
        gridView.setAdapter(adapter);
        adapter.setItems(items);

    }
}
