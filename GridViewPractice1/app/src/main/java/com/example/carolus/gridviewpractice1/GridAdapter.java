package com.example.carolus.gridviewpractice1;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by carolus on 2017-05-28.
 */

public class GridAdapter extends BaseAdapter {

    private ArrayList<GridData> items =new ArrayList<>();


    public void setItems(ArrayList<GridData> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView==null){
            convertView=new GridViewView(parent.getContext());
        }


        ((GridViewView)convertView).setData(items.get(position));

        return convertView;
    }
}
