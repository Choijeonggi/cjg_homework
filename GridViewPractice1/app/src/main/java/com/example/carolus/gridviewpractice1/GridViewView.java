package com.example.carolus.gridviewpractice1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by carolus on 2017-05-28.
 */

public class GridViewView extends FrameLayout {


    private TextView gridsource;
    private Context context;
    private GridData data;


    public GridViewView(@NonNull Context context) {
        super(context);
        this.context=context;
        View view = LayoutInflater.from(context).inflate(R.layout.view,this,true);

        gridsource= (TextView) view.findViewById(R.id.gridsource);
    }

    public void setData(GridData data) {
        this.data = data;
        gridsource.setText(data.string);
    }
}
