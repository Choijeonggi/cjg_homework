package com.example.carolus.memoapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by carolus on 2017-04-22.
 */

public class WriteMemoPage extends AppCompatActivity {

    Button memoPageBackToListButton;
    TextView memoPageTitle, memoPageWritePlace;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(Activity.RESULT_CANCELED);

        setContentView(R.layout.memo_page);
        memoPageTitle= (TextView) findViewById(R.id.memoPageTitle);

        Intent intent = getIntent();
        String value = intent.getStringExtra("title");
        memoPageTitle.setText(value);

        memoPageBackToListButton = (Button) findViewById(R.id.memoPageBackToListButton);
        memoPageWritePlace = (TextView) findViewById(R.id.memoPageWritePlace);

        memoPageBackToListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = memoPageWritePlace.getText().toString();
                Intent resultIntent = new Intent(WriteMemoPage.this, MainActivity.class);
                resultIntent.putExtra("result", result);
                setResult(Activity.RESULT_OK, resultIntent);

                finish();
            }
        });


    }
}
