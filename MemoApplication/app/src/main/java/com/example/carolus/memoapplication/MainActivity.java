package com.example.carolus.memoapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView memoTitleList;
    Button writePageButton;


//    private static final int MENU_DEFAULT_VALUE=0;
//    private static final int MENU_MOVE_WRITEPAGE=0;
//    private static final int REQ_NEW_MEMO=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        memoTitleList = (ListView) findViewById(R.id.memoTitleList);
        writePageButton= (Button) findViewById(R.id.writePageButton);

        writePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = memoTitleList.getSelectedItem().toString();

                Intent intent = new Intent();
                intent.putExtra("title", value);
                intent.setClass(MainActivity.this, WriteMemoPage.class);

                startActivityForResult(intent,0);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==0&&requestCode== Activity.RESULT_OK){
            String reply = data.getStringExtra("result");

        }
    }


    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu){
//        MenuItem moveWritePage = menu.add(MENU_DEFAULT_VALUE,MENU_MOVE_WRITEPAGE,MENU_MOVE_WRITEPAGE,"write");
//        moveWritePage.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item){
//        switch (item.getItemId()){
//            case MENU_MOVE_WRITEPAGE:
//                startActivityForResult(new Intent(MainActivity.this, WriteMemoPage.class),REQ_NEW_MEMO);
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode==REQ_NEW_MEMO){
//            //do coding continue
//        }
//    }
}
