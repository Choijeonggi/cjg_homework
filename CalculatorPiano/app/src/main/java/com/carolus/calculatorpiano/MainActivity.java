package com.carolus.calculatorpiano;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText resultText;
    Button allClearBtn, button0,  button7,button8,button9,button1,button2,button3,button4,button5,button6,toPiano;
    Button  equalBtn, plusBtn, minusBtn,divisionBtn,timesBtn;

    String val;

    int cal;
    int P=10;
    int M=11;
    int D=12;
    int T=13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toPiano= (Button) findViewById(R.id.toPiano);
        resultText= (EditText) findViewById(R.id.resultText);
        allClearBtn= (Button) findViewById(R.id.allClearBtn);
        button0= (Button) findViewById(R.id.button0);
        equalBtn= (Button) findViewById(R.id.equalBtn);
        plusBtn= (Button) findViewById(R.id.plusBtn);
        button7= (Button) findViewById(R.id.button7);
        button8= (Button) findViewById(R.id.button8);
        button9= (Button) findViewById(R.id.button9);
        minusBtn= (Button) findViewById(R.id.minusBtn);
        button4= (Button) findViewById(R.id.button4);
        button5= (Button) findViewById(R.id.button5);
        button6= (Button) findViewById(R.id.button6);
        timesBtn= (Button) findViewById(R.id.timesBtn);
        button1= (Button) findViewById(R.id.button1);
        button2= (Button) findViewById(R.id.button2);
        button3= (Button) findViewById(R.id.button3);
        divisionBtn= (Button) findViewById(R.id.divisionBtn);

        val="";

        toPiano.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent piano = new Intent(MainActivity.this, PianoActivity.class);
                startActivity(piano);

            }
        });



        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+0);
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+1);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+2);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+3);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+4);
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+5);
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+6);
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+7);
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+8);
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultText.setText(resultText.getText().toString()+9);
            }
        });


        allClearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                val="";
                resultText.setText("");

            }
        });


        plusBtn.setOnClickListener(calculator);
        minusBtn.setOnClickListener(calculator);
        divisionBtn.setOnClickListener(calculator);
        timesBtn.setOnClickListener(calculator);
        equalBtn.setOnClickListener(calculator);


    }


    Button.OnClickListener calculator = new Button.OnClickListener(){

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case (R.id.plusBtn):
                    val=resultText.getText().toString();
                    resultText.setText("");
                    cal =P;
                    break;
                case (R.id.minusBtn):
                    val=resultText.getText().toString();
                    resultText.setText("");
                    cal =M;
                    break;
                case (R.id.divisionBtn):
                    val=resultText.getText().toString();
                    resultText.setText("");
                    cal =D;
                    break;
                case (R.id.timesBtn):
                    val=resultText.getText().toString();
                    resultText.setText("");
                    cal =T;
                    break;
                case (R.id.equalBtn):
                    if (cal ==P){
                        resultText.setText(""+(Double.parseDouble(val)+Double.parseDouble(resultText.getText().toString())));
                    }else if (cal ==M){
                        resultText.setText(""+ (Double.parseDouble(val)-Double.parseDouble(resultText.getText().toString())));
                    }else if (cal ==D){
                        resultText.setText(""+(Double.parseDouble(val)/Double.parseDouble(resultText.getText().toString())));
                    }else if (cal ==T){
                        resultText.setText(""+(Double.parseDouble(val)*Double.parseDouble(resultText.getText().toString())));
                    }

                    resultText.getText().toString();
                    break;
            }
        }
    };
}
//