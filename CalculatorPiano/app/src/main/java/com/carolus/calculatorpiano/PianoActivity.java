package com.carolus.calculatorpiano;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by carolus on 2017-04-06.
 */

public class PianoActivity extends AppCompatActivity {

    Button douup,reup,faup,solup,laup,dou,re,me,fa,sol,la,si,dou2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_piano);

        douup= (Button) findViewById(R.id.douup);
        reup= (Button) findViewById(R.id.reup);
        faup= (Button) findViewById(R.id.faup);
        solup= (Button) findViewById(R.id.solup);
        laup= (Button) findViewById(R.id.laup);
        dou= (Button) findViewById(R.id.dou);
        re= (Button) findViewById(R.id.re);
        me= (Button) findViewById(R.id.me);
        fa= (Button) findViewById(R.id.fa);
        sol= (Button) findViewById(R.id.sol);
        la= (Button) findViewById(R.id.la);
        si= (Button) findViewById(R.id.si);
        dou2= (Button) findViewById(R.id.dou2);



        douup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"도#",Toast.LENGTH_SHORT).show();
            }
        });
        reup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"레#",Toast.LENGTH_SHORT).show();
            }
        });
        faup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"파#",Toast.LENGTH_SHORT).show();
            }
        });
        solup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"솔#",Toast.LENGTH_SHORT).show();
            }
        });
        laup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"라#",Toast.LENGTH_SHORT).show();
            }
        });
        dou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"도",Toast.LENGTH_SHORT).show();
            }
        });
        re.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"레",Toast.LENGTH_SHORT).show();
            }
        });
        me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"미",Toast.LENGTH_SHORT).show();
            }
        });
        fa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"파",Toast.LENGTH_SHORT).show();
            }
        });
        sol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"솔",Toast.LENGTH_SHORT).show();
            }
        });
        la.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"라",Toast.LENGTH_SHORT).show();
            }
        });
        si.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"시",Toast.LENGTH_SHORT).show();
            }
        });
        dou2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PianoActivity.this,"도",Toast.LENGTH_SHORT).show();
            }
        });


    }
}
