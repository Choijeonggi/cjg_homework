package com.example.carolus.databasetest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by carolus on 2017-07-23.
 */

public class UserView extends FrameLayout {

    TextView id, pw, name, age;
    ImageButton update, delete;


    public UserView(@NonNull Context context) {
        super(context);

        View view = LayoutInflater.from(context).inflate(R.layout.user_view, this, true);

        id = (TextView) view.findViewById(R.id.id);
        pw = (TextView) view.findViewById(R.id.pw);
        name = (TextView) view.findViewById(R.id.name);
        age = (TextView) view.findViewById(R.id.age);
        update = (ImageButton) view.findViewById(R.id.updateBtn);
        delete = (ImageButton) view.findViewById(R.id.deleteBtn);

        update.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onUserViewBtnClickListener != null) {
                    onUserViewBtnClickListener.onUpdate();
                }
            }
        });

        delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onUserViewBtnClickListener != null) {
                    onUserViewBtnClickListener.onDelete();
                }
            }
        });
    }

    public void setUser(User user) {
        id.setText(user.getId());
        pw.setText(user.getPw());
        name.setText(user.getName());
        age.setText(String.valueOf(user.getAge()));

    }

    private OnUserViewBtnClickListener onUserViewBtnClickListener;

    public void setOnUserViewBtnClickListener(OnUserViewBtnClickListener onUserViewBtnClickListener) {
        this.onUserViewBtnClickListener = onUserViewBtnClickListener;
    }

    public interface OnUserViewBtnClickListener {
        void onUpdate();

        void onDelete();

    }

}
