package com.example.carolus.databasetest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    ListView dataSheet;

    UserAdapter userAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataSheet= (ListView) findViewById(R.id.dataSheet);

        userAdapter=new UserAdapter();
        dataSheet.setAdapter(userAdapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuItem add= menu.add(0,0,0,"Add");
        add.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 0:
                Intent intent = new Intent(MainActivity.this,SignUpActivity.class);
                startActivity(intent);
        }
        return true;
    }
}
