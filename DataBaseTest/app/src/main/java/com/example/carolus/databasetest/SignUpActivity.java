package com.example.carolus.databasetest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import io.realm.Realm;

public class SignUpActivity extends AppCompatActivity {

    EditText wid,wpw,wname,wage;
    Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);



        wid= (EditText) findViewById(R.id.writeId);
        wpw= (EditText) findViewById(R.id.writePw);
        wname= (EditText) findViewById(R.id.writeName);
        wage= (EditText) findViewById(R.id.writeAge);
        saveBtn= (Button) findViewById(R.id.saveBtn);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Realm realm=Realm.getDefaultInstance();


                User user = new User(
                        wid.getText().toString(),
                        wpw.getText().toString(),
                        wname.getText().toString(),
                        Integer.parseInt(wage.getText().toString())
                );

                realm.beginTransaction();

                realm.copyToRealmOrUpdate(user);

                realm.commitTransaction();


                finish();
            }
        });

        if (getIntent().hasExtra("update")){
            User user=getIntent().getParcelableExtra("update");
            wid.setText(user.getId());
            wpw.setText(user.getPw());
            wname.setText(user.getName());
            wage.setText(""+user.getAge());
        }



    }
}
