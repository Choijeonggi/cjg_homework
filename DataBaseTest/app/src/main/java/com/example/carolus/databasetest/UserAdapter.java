package com.example.carolus.databasetest;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by carolus on 2017-07-23.
 */

public class UserAdapter extends BaseAdapter {

    private RealmResults<User> users;
    private Realm realm;


    private RealmChangeListener<RealmResults<User>> realmChangeListener = new RealmChangeListener<RealmResults<User>>() {
        @Override
        public void onChange(RealmResults<User> element) {
            users = element;
            notifyDataSetChanged();
        }
    };

    public UserAdapter() {
        realm = Realm.getDefaultInstance();
        users = realm.where(User.class).findAll();
        users.addChangeListener(realmChangeListener);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public User getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView,final ViewGroup parent) {
        UserView userView = (UserView) convertView;

        if (userView == null) {
            userView = new UserView(parent.getContext());
        }
        userView.setUser(users.get(position));
        userView.setOnUserViewBtnClickListener(new UserView.OnUserViewBtnClickListener() {
            @Override
            public void onUpdate() {
                //mvc위반
                Intent intent =new Intent(parent.getContext(),SignUpActivity.class);
                intent.putExtra("update",users.get(position));
                parent.getContext().startActivity(intent);
            }

            @Override
            public void onDelete() {
                realm.beginTransaction();
                users.get(position).deleteFromRealm();
                realm.commitTransaction();
                notifyDataSetChanged();
            }
        });
        return userView;
    }
}
