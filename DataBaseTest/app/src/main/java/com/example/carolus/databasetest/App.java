package com.example.carolus.databasetest;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by carolus on 2017-07-23.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
    }
}
