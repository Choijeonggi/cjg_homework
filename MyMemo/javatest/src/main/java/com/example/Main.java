package com.example;

public class Main {


    public static void main(String[] args){
        //출발점
        Person david = new Person();
        david.name = "david";
        david.age= 2;
        david.height = 50.0f;
        david.sex = Person.WOMAN;

        Person susan = new Person();
        susan.name = "susan";
        susan.age = 30;
        susan.height = 182.5f;
        susan.sex = Person.MAN;

        david.introduceMyself();
        susan.introduceMyself();

        Person emma = new Person("emma", 30, 165f,Person.WOMAN);
        emma.introduceMyself();

        Person eddi = new Person("eddi");
        eddi.introduceMyself();
    }
}
