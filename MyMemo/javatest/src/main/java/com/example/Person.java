package com.example;

import java.util.Random;

/**
 * Created by carolus on 2017-05-07.
 */

public class Person {

    public static final boolean MAN = true;
    public static final boolean WOMAN = false;



    String name;
    int age;
    float height;
    boolean sex;

    //default Constructor
    public Person(){

    }

    public Person(String name,int age,float height,boolean sex){
      this.name= name;
        this.age=age;
        this.height=height;
        this.sex=sex;
    }

    //생성자 만들기! 이름만 입력받고 나머지 프로퍼티(속성 또는 멤버변수)는 랜덤하게 생성
    public Person(String name){
        this.name=name;
        Random r = new Random();
        age = r.nextInt(100);
        height = r.nextFloat()*100;
        sex = r.nextBoolean();

        System.out.println("result:"+v);
    }


    void introduceMyself(){
        if (sex == MAN){
            System.out.print("my name is "+name+"\n  my age is " + age +"\n my height is" +height+"\n I'm man\n" +"\n");

        }else {
            System.out.print("my name is "+name+"\n  my age is " + age +"\n my height is" +height+"\n I'm woman\n\n" );

        }
    }
}
