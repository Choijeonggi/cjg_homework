package com.example.carolus.mymemo;

/**
 * Created by carolus on 2017-05-07.
 */

public class ThirdActivity {
    int age = 26;//인트라는 자료형에 넘버라는 변수 안에 10이라는 값을 초기화 한 것
    String name = "choijeonggi";

    /*//이렇게 생긴게 메소드야,,*/
    public void setMyname(String name){
        this.name=name;
    }

    public int getMyage(){
        return age+1;
    }
    //addAge 라는 메소드를 만들고 나이에 n만큼 더한값을 리턴 하시오

    public int addAge(int n){

        return age+n;
    }
    String getMyname(){

        return name;
    }
}
