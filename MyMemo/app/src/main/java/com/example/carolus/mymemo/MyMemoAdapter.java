package com.example.carolus.mymemo;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by carolus on 2017-04-29.
 */

public class MyMemoAdapter extends BaseAdapter {

    private ArrayList<MemoItemData> memoitems = new ArrayList<>();

//    SubMemoWriteActivity SMWA;

    public void setMemoItems(ArrayList<MemoItemData> memoitems){
        this.memoitems=memoitems;
        notifyDataSetChanged();
    }


    public void addMemo(MemoItemData memoitemdata){


        memoitems.add(memoitemdata);
        notifyDataSetChanged();

    }

    public void deleteMemo(int position){
        memoitems.remove(position);
        notifyDataSetChanged();
    }




    @Override
    public int getCount() {
        return memoitems.size();
    }

    @Override
    public MemoItemData getItem(int position) {
//        Object obj = memoitems.get(position);
//        MemoItemData m = obj;
        return memoitems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return memoitems.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView==null){
            convertView = new MemoView(parent.getContext());

        }

        ((MemoView)convertView).setData(memoitems.get(position));

        return convertView;
    }
}
