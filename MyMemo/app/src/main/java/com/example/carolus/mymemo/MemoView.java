package com.example.carolus.mymemo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by carolus on 2017-05-17.
 */

public class MemoView extends FrameLayout {

    private TextView viewTitle, viewContents;

    private Context context;
    private MemoItemData data;

    public MemoView(@NonNull Context context) {
        super(context);
        this.context=context;
        View view= LayoutInflater.from(context).inflate(R.layout.memo_view_source,this,true);

        viewTitle= (TextView) view.findViewById(R.id.viewTitle);
        viewContents= (TextView) view.findViewById(R.id.viewContents);

    }


    public void setData(MemoItemData data){
        this.data=data;
        viewTitle.setText(data.memoTitleData);
        viewContents.setText(data.memoContentsData);
    }
}
