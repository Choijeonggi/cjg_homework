package com.example.carolus.mymemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainMemoActivity extends AppCompatActivity {
    public static final String TAG = MainMemoActivity.class.getSimpleName();

    ListView main_list;

    String[] data = new String[]{};


    MyMemoAdapter adapter;

    private static final int MENU_DEFAULT_VALUE = 0;
    private static final int MENU_MOVE_WRITEPAGE = 0;
    private static final int REQ_NEW_MEMO = 1;
    private static final int REQ_OLD_MEMO=9;

    public static final String KEY_TITLE_AT_lIST= "keyTitleAtList";
    public static final String KEY_CONTENTS_AT_LIST="KeyContentsAtList";
    public static final String KEY_POSITION="KeyPosition";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_memo);

//

        main_list = (ListView) findViewById(R.id.main_list);

        adapter = new MyMemoAdapter();

        main_list.setAdapter(adapter);



        main_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MemoItemData titleContentsData = adapter.getItem(position);

                Intent intentMemoData = new Intent();
                intentMemoData.putExtra(KEY_TITLE_AT_lIST,titleContentsData.memoTitleData);
                intentMemoData.putExtra(KEY_CONTENTS_AT_LIST,titleContentsData.memoContentsData);
                intentMemoData.putExtra(KEY_POSITION,position);
                intentMemoData.setClass(MainMemoActivity.this,SubMemoWriteActivity.class);

//

//                String strdContents = adapter.getItem(position).toString();
//
//                Intent intentdataContents = new Intent();
//                intentdataContents.putExtra("keyContentsAtList",strdContents);
//                intentdataContents.setClass(MainMemoActivity.this,SubMemoWriteActivity.class);




                startActivityForResult(intentMemoData,REQ_OLD_MEMO);







            }
        });

        main_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {



                adapter.deleteMemo(position);
                return false;
            }
        });








//        Intent intent = getIntent();
//        String titleData=intent.getStringExtra(SubMemoWriteActivity.KEY_TITLE);
//        Toast.makeText(MainMemoActivity.this, titleData, Toast.LENGTH_SHORT).show();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem moveWritePage = menu.add(MENU_DEFAULT_VALUE, MENU_MOVE_WRITEPAGE, MENU_MOVE_WRITEPAGE, "write");
        moveWritePage.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_MOVE_WRITEPAGE:
                Log.e(TAG, "MENU_MOVE_WRITEPAGE");
                startActivityForResult(new Intent(MainMemoActivity.this, SubMemoWriteActivity.class), REQ_NEW_MEMO);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       if (requestCode==REQ_NEW_MEMO&&resultCode==RESULT_OK){

           String titleData = data.getStringExtra(KEY_TITLE_AT_lIST);
           Toast.makeText(MainMemoActivity.this, titleData, Toast.LENGTH_SHORT).show();

           String contentsData = data.getStringExtra(KEY_CONTENTS_AT_LIST);
           Toast.makeText(MainMemoActivity.this, contentsData , Toast.LENGTH_SHORT).show();



           MemoItemData memoitemdata = new MemoItemData();
           memoitemdata.memoTitleData = titleData;
           memoitemdata.memoContentsData=contentsData;


           adapter.addMemo(memoitemdata);

           Log.e(TAG, "sub -> main ; memodata/save at adapter" );
//           adapter.addtitle(contentsData);


       }else if (requestCode==REQ_OLD_MEMO&&resultCode==RESULT_OK){

           int position = data.getIntExtra(KEY_POSITION,-1);
           adapter.deleteMemo(position);
           String titleDataOld = data.getStringExtra(KEY_TITLE_AT_lIST);
           String contentsDataOld = data.getStringExtra(KEY_CONTENTS_AT_LIST);


           MemoItemData memoitemdataOld = new MemoItemData();
           memoitemdataOld.memoTitleData = titleDataOld;
           memoitemdataOld.memoContentsData=contentsDataOld;


           adapter.addMemo(memoitemdataOld);


       }
    }
}
