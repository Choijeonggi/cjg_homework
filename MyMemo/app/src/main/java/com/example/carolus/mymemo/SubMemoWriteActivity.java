package com.example.carolus.mymemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by carolus on 2017-04-27.
 */

public class SubMemoWriteActivity extends AppCompatActivity {

    public static final String TAG = SubMemoWriteActivity.class.getSimpleName();

    private static final int MENU_DEFAULT_VALUE = 0;
    private static final int MENU_MOVE_MAIN = 0;
    private static final int REQ_NEW_LIST = 1;

    EditText sub_contents, sub_title;

    int position;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(Activity.RESULT_CANCELED);
        setContentView(R.layout.activity_sub_memo_write);

        sub_title= (EditText) findViewById(R.id.sub_title);

        sub_contents = (EditText) findViewById(R.id.sub_contents);


        Intent intent = getIntent();
        String receiveTitle = intent.getStringExtra(MainMemoActivity.KEY_TITLE_AT_lIST);
        sub_title.setText(receiveTitle);

        String receiveContents = intent.getStringExtra(MainMemoActivity.KEY_CONTENTS_AT_LIST);
        sub_contents.setText(receiveContents);

       if(intent.hasExtra(MainMemoActivity.KEY_POSITION)){
           position = intent.getIntExtra(MainMemoActivity.KEY_POSITION,-1);
       }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem moveWritePage = menu.add(MENU_DEFAULT_VALUE, MENU_MOVE_MAIN, MENU_MOVE_MAIN, "back");
        moveWritePage.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    //하고싶은것?
    //
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_MOVE_MAIN:

                String strContents = sub_contents.getText().toString();
                String strTitle = sub_title.getText().toString();
                Intent intentPassMemo = new Intent(SubMemoWriteActivity.this,MainMemoActivity.class);
                intentPassMemo.putExtra(MainMemoActivity.KEY_CONTENTS_AT_LIST,strContents);
                intentPassMemo.putExtra(MainMemoActivity.KEY_TITLE_AT_lIST,strTitle);
                intentPassMemo.putExtra(MainMemoActivity.KEY_POSITION,position);

                setResult(RESULT_OK,intentPassMemo);
                finish();



                break;
        }
        return super.onOptionsItemSelected(item);
    }



}
